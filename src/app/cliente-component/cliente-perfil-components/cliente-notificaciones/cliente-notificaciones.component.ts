import { Component, OnInit } from '@angular/core';
import { NotificacionesService } from '../../../globals-services/notificaciones.service';
import { Socket } from 'ngx-socket-io';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cliente-notificaciones',
  templateUrl: './cliente-notificaciones.component.html',
  styleUrls: ['./cliente-notificaciones.component.scss']
})
export class ClienteNotificacionesComponent implements OnInit {
  nuevos: any;
  notificaciones: any;
  constructor(    
    private notificacionesService:NotificacionesService,
    private socket:Socket,
    private router:Router) { }

  ngOnInit() {
    this.socket.on('nueva_notificacion_r',()=>{
      this.getNotificaciones()
    })

    this.getNotificaciones()
  }
  getNotificaciones() {
    this.notificacionesService.misNotificaciones(localStorage.getItem('id')).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.notificaciones = resp.notificaciones
        this.nuevos = resp.nuevos
      }
    })
  }

  verNotificacion(id){
      this.router.navigate(['/cliente/servicio/postulaciones'],{queryParams:{id}})
  }
}
 