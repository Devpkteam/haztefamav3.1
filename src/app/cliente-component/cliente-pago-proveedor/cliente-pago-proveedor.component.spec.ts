import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientePagoProveedorComponent } from './cliente-pago-proveedor.component';

describe('ClientePagoProveedorComponent', () => {
  let component: ClientePagoProveedorComponent;
  let fixture: ComponentFixture<ClientePagoProveedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientePagoProveedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientePagoProveedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
