import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ServiciosService } from '../../../globals-services/solicitud.service';
import { CategoriaService } from '../../../globals-services/categoria.service';
import { SubcategoriaService } from '../../../globals-services/subcategoria.service';

@Component({
  selector: 'app-nueva-solicitud',
  templateUrl: './nueva-solicitud.component.html',
  styleUrls: ['./nueva-solicitud.component.css']
})
export class NuevaSolicitudComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevaSolicitudComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private solicitudServices:ServiciosService,
    private categoriasServices: CategoriaService,
    private subcategoria: SubcategoriaService
  ) { }

  categorias = []
  subcategorias = []

  ngOnInit(): void {
    
    this.categoriasServices.getCategorias().subscribe((resp:any) =>{
      if(resp.ok){
        this.categorias = resp.categorias        
      }
    })

    this.subcategoria.getSubcategorias().subscribe((resp:any) =>{
      if(resp.ok){
        this.subcategorias = resp.subcategorias
      }
    })

  }

  
  solicitudesForm = this.fb.group({
    usuario: [localStorage.getItem('id'), Validators.required],
    nombre: ['', [Validators.required]],
    descripcion: ['', [Validators.required]],
    max_price: ['', [Validators.required]],
    min_price: ['', [Validators.required]],
    categoria: ['', [Validators.required]],
    subcategoria: ['', [Validators.required]],
    lugar_evento: ['', [Validators.required]],
    fecha_evento: ['', [Validators.required]]
    
  },{validators:this.checkvalues})

  checkvalues(form: FormGroup){
    let min_price = form.controls.min_price.value;
    let max_price = form.controls.max_price.value;    
  
    if(min_price > max_price){
      form.controls.min_price.setErrors({ 'repeatInvalid' : true })      
    }
    return null
  }

  aceptar(){
    this.solicitudServices.setServicios(this.solicitudesForm.value).subscribe((resp:any) =>{
      
      if(resp.ok){
        this.dialogRef.close(true)
      }
    })
  }

  onNoClick(): void {
    this.dialogRef.close(false)
  }

}
