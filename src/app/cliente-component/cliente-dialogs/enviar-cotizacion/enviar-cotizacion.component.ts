import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CotizacionService } from '../../../globals-services/cotizacion.service';

@Component({
  selector: 'app-enviar-cotizacion',
  templateUrl: './enviar-cotizacion.component.html',
  styleUrls: ['./enviar-cotizacion.component.scss']
})
export class EnviarCotizacionComponent implements OnInit {

  constructor(
    private fb:FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EnviarCotizacionComponent>,
    private cotizacionService:CotizacionService
  ) { }

  ngOnInit() {
  }

  cotizacionForm = this.fb.group({
    motivo:["",[Validators.required]],
    monto:[0,[Validators.required]],
    postulacion:[this.data.id,[Validators.required]],
    servicio:[this.data.servicio,[Validators.required]],
  })

  aceptar(){  
    this.cotizacionService.crearCotizacion(this.cotizacionForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true)
      }
    })
  }

  onNoClick(){
    this.dialogRef.close(false)
  }

}
