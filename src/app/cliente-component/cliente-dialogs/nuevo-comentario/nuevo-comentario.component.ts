import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { FormBuilder, Validators } from "@angular/forms";
import { ComentariosService } from "../../../globals-services/comentarios.service";
@Component({
  selector: "app-nuevo-comentario",
  templateUrl: "./nuevo-comentario.component.html",
  styleUrls: ["./nuevo-comentario.component.scss"],
})
export class NuevoComentarioComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<NuevoComentarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private comentarioService: ComentariosService
  ) {}

  ngOnInit() {}

  // tslint:disable-next-line: member-ordering
  comentarioForm = this.fb.group({
    comentario: ["", [Validators.required]],
    usuario: [localStorage.getItem("id"), [Validators.required]],
  });

  aceptar() {
    this.comentarioService
      .newComentario(this.comentarioForm.value)
      .subscribe((resp: any) => {
        if (resp.ok) {
          this.dialogRef.close(true);
        }
      });
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }
}
