import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { ComentariosService } from '../../../globals-services/comentarios.service';

@Component({
  selector: 'app-editar-comentario',
  templateUrl: './editar-comentario.component.html',
  styleUrls: ['./editar-comentario.component.scss']
})
export class EditarComentarioComponent implements OnInit {


  constructor(
    
    public dialogRef: MatDialogRef<EditarComentarioComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      private fb:FormBuilder,
      private comentarioService:ComentariosService
    ) { }
  
    ngOnInit() {
    }
  
    comentarioForm = this.fb.group({
      comentario:[this.data.comentario,[Validators.required]],
      idUser:[localStorage.getItem('id'),Validators.required]
    })
  
  
    aceptar(){
     
      this.comentarioService.editarComentario(this.data._id,this.comentarioForm.value).subscribe((resp:any)=>{
        if(resp.ok){
          this.dialogRef.close(true);
        }
      })
      
    }
    
    onNoClick(): void {
      this.dialogRef.close(false);
    }
  
}
