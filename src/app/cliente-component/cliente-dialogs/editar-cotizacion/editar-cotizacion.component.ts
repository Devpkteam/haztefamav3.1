import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { CotizacionService } from "../../../globals-services/cotizacion.service";

@Component({
  selector: "app-editar-cotizacion",
  templateUrl: "./editar-cotizacion.component.html",
  styleUrls: ["./editar-cotizacion.component.css"],
})
export class EditarCotizacionComponent implements OnInit {
  motivo: any;
  monto: any;
  id:any;
  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditarCotizacionComponent>,
    private cotizacionService: CotizacionService
  ) {}

  ngOnInit(): void {
    this.id = this.data.id;
  }
  cotizacionForm = this.fb.group({
    motivo: [this.data.motivo, [Validators.required]],
    monto: [this.data.monto, [Validators.required]],
  });
  aceptar() {
    this.cotizacionService
      .editarCotizacion(this.id,this.cotizacionForm.value)
      .subscribe((resp: any) => {
        if (resp.ok) {
          this.dialogRef.close(true);
        }
      });
  }

  onNoClick() {
    this.dialogRef.close(false);
  }
}
