import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ServiciosService } from '../../../globals-services/solicitud.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CategoriaService } from '../../../globals-services/categoria.service';
import { SubcategoriaService } from '../../../globals-services/subcategoria.service';

@Component({
  selector: 'app-editar-solicitud-component',
  templateUrl: './editar-solicitud.component.html',
  styleUrls: ['./editar-solicitud.component.css']
})
export class EditarSolicitudComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private solicitudesServices: ServiciosService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditarSolicitudComponent>,
    private categoriasServices: CategoriaService,
    private subcategoriasServices: SubcategoriaService  
  ) { }

  ngOnInit(): void {
    this.categoriasServices.getCategorias().subscribe((resp:any) =>{
      if(resp.ok){
        this.categorias = resp.categorias        
      }
    })
    this.subcategoriasServices.getSubcategorias().subscribe((resp:any) =>{
      if(resp.ok){
        this.subcategorias = resp.subcategorias        
      }
    })
  }

  solicitudesForm = this.fb.group({
    idAdmin: [localStorage.getItem('id'), Validators.required],
    nombre: [this.data.nombre, [Validators.required]],
    descripcion: [this.data.descripcion, [Validators.required]],
    fecha_modificacion: [Date.now],
    max_price: [this.data.max_price, [Validators.required]],
    min_price: [this.data.min_price, [Validators.required]],
    categoria: [this.data.categoria.id, [Validators.required]],
    subcategoria: [this.data.subcategoria.id, [Validators.required]], 
    lugar_evento: [this.data.lugar_evento, [Validators.required]],
    fecha_evento: [this.data.fecha_evento, [Validators.required]]
    
  },{validators:this.checkvalues})

  checkvalues(form: FormGroup){
    let min_price = form.controls.min_price.value;
    let max_price = form.controls.max_price.value;
  
    if(min_price > max_price){
      form.controls.min_price.setErrors({ 'repeatInvalid' : true })      
    }
    return null
  }

  categorias = [];
  subcategorias = [];

  aceptar(){
    this.solicitudesServices.updateServicios(this.data._id, this.solicitudesForm.value).subscribe((resp: any) => {
      if(resp.ok){
        this.dialogRef.close(true);
      }
    })
  }

  onNoClick():void {
    this.dialogRef.close(false);
  }

}
