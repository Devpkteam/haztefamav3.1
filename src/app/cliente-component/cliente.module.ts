import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';

import { DemoMaterialModule } from '../demo-material-module';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule, CLASS_NAME } from '@angular/flex-layout';

import { ClienteRoutes } from './cliente.routing';
import { ClienteMembresiasComponent } from './cuenta-components/cliente-membresias/cliente-membresias.component';
import { ClienteInformacionComponent } from './cuenta-components/cliente-informacion/cliente-informacion.component';
import { ClienteSolicitudServicioComponent } from './solicitar-servicio-components/cliente-solicitud-servicio/cliente-solicitud-servicio.component';
import { ClienteSolicitudProveedorComponent } from './solicitar-servicio-components/cliente-solicitud-proveedor/cliente-solicitud-proveedor.component';
import { ClientePerfilComponent } from './cliente-perfil-components/cliente-perfil/cliente-perfil.component';
import { ClienteBalanceComponent } from './cliente-perfil-components/cliente-balance/cliente-balance.component';
import { ClienteNotificacionesComponent } from './cliente-perfil-components/cliente-notificaciones/cliente-notificaciones.component';
import { ClienteProveedoresFavoritosComponent } from './cliente-proveedores-favoritos/cliente-proveedores-favoritos.component';
import { ClienteCalificacionesComponent } from './cliente-calificaciones/cliente-calificaciones.component';
import { ClienteAtencionAlClienteComponent } from './cliente-atencion-al-cliente/cliente-atencion-al-cliente.component';
import { ComentariosYSugerenciasComponent } from '../admin-component/servicios-clientes-components/comentarios-y-sugerencias/comentarios-y-sugerencias.component';
import { ClienteComentariosYSugerenciasComponent } from './cliente-comentarios-y-sugerencias/cliente-comentarios-y-sugerencias.component';
import { NuevoComentarioComponent } from './cliente-dialogs/nuevo-comentario/nuevo-comentario.component';
import { EditarComentarioComponent } from './cliente-dialogs/editar-comentario/editar-comentario.component';
import { EditarSolicitudComponent } from './cliente-dialogs/editar-solicitud/editar-solicitud.component';
import { NuevaSolicitudComponent } from './cliente-dialogs/nueva-solicitud/nueva-solicitud.component';
import { NuevoClienteSolicitudServicioComponent } from './solicitar-servicio-components/nuevo-cliente-solicitud-servicio/nuevo-cliente-solicitud-servicio.component';
import { ClientePostulacionesServicioComponent } from './solicitar-servicio-components/cliente-postulaciones-servicio/cliente-postulaciones-servicio.component';
import { ClientePostulacionesServicioChatComponent } from './solicitar-servicio-components/cliente-postulaciones-servicio-chat/cliente-postulaciones-servicio-chat.component';
import { EnviarCotizacionComponent } from './cliente-dialogs/enviar-cotizacion/enviar-cotizacion.component';
import { EditarCotizacionComponent } from './cliente-dialogs/editar-cotizacion/editar-cotizacion.component';
// import { ServiciosComponent } from './servicios/servicios.component';
import {GoogleMapsModule} from '@angular/google-maps'; 
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { ClientePagoProveedorComponent } from './cliente-pago-proveedor/cliente-pago-proveedor.component';




@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ClienteRoutes),
    DemoMaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule,
    GoogleMapsModule,
    GooglePlaceModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  entryComponents: [],
  declarations: [
    ClienteMembresiasComponent,
    ClienteInformacionComponent,
    ClienteSolicitudServicioComponent,
    ClienteSolicitudProveedorComponent,
    ClientePerfilComponent,
    ClienteBalanceComponent,
    ClienteNotificacionesComponent,
    ClienteSolicitudProveedorComponent,
    ClienteProveedoresFavoritosComponent,
    ClienteCalificacionesComponent,
    ClienteAtencionAlClienteComponent,
    ClienteComentariosYSugerenciasComponent,
    NuevoComentarioComponent,
    EditarComentarioComponent,
    EditarSolicitudComponent,
    NuevaSolicitudComponent,
    NuevoClienteSolicitudServicioComponent,  
    ClientePostulacionesServicioComponent,
    ClientePostulacionesServicioChatComponent,
    EnviarCotizacionComponent,
    EditarCotizacionComponent,
    ClientePagoProveedorComponent,

    
    
  ]
})
export class ClienteComponentsModule {}
