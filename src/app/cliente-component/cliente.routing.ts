import { Routes } from '@angular/router';
import { AuthGuardAdminService } from '../guards/auth-guard-admin.service';
import { InicioClienteComponent } from './inicio-cliente/inicio-cliente.component';
import { AuthGuardClientService } from '../guards/auth-guard-client.service';
import { ClienteInformacionComponent } from './cuenta-components/cliente-informacion/cliente-informacion.component';
import { ClienteMembresiasComponent } from './cuenta-components/cliente-membresias/cliente-membresias.component';
import { ClienteSolicitudServicioComponent } from './solicitar-servicio-components/cliente-solicitud-servicio/cliente-solicitud-servicio.component';
import { ClienteSolicitudProveedorComponent } from './solicitar-servicio-components/cliente-solicitud-proveedor/cliente-solicitud-proveedor.component';
import { ClientePerfilComponent } from './cliente-perfil-components/cliente-perfil/cliente-perfil.component';
import { ClienteBalanceComponent } from './cliente-perfil-components/cliente-balance/cliente-balance.component';
import { ClienteNotificacionesComponent } from './cliente-perfil-components/cliente-notificaciones/cliente-notificaciones.component';
import { ClienteProveedoresFavoritosComponent } from './cliente-proveedores-favoritos/cliente-proveedores-favoritos.component';
import { ClienteCalificacionesComponent } from './cliente-calificaciones/cliente-calificaciones.component';
import { ClienteAtencionAlClienteComponent } from './cliente-atencion-al-cliente/cliente-atencion-al-cliente.component';
import { ComentariosYSugerenciasComponent } from '../admin-component/servicios-clientes-components/comentarios-y-sugerencias/comentarios-y-sugerencias.component';
import { ClienteComentariosYSugerenciasComponent } from './cliente-comentarios-y-sugerencias/cliente-comentarios-y-sugerencias.component';
import { NuevoClienteSolicitudServicioComponent } from './solicitar-servicio-components/nuevo-cliente-solicitud-servicio/nuevo-cliente-solicitud-servicio.component';
import { ClientePostulacionesServicioComponent } from './solicitar-servicio-components/cliente-postulaciones-servicio/cliente-postulaciones-servicio.component';
import { ClientePostulacionesServicioChatComponent } from './solicitar-servicio-components/cliente-postulaciones-servicio-chat/cliente-postulaciones-servicio-chat.component';
import { NoVerificadoComponent } from '../login-components/no-verificado/no-verificado.component';
import { ClientePagoProveedorComponent } from './cliente-pago-proveedor/cliente-pago-proveedor.component';


export const ClienteRoutes: Routes = [
  {
    path: 'inicio',
    component: InicioClienteComponent,
    data:{
      title:'Inicio',
      breadcrumbs:['Inicio']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'mi-cuenta/informacion',
    component: ClienteInformacionComponent,
    data:{
      title:'Información',
      breadcrumbs:['Mi cuenta','Información']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'mi-cuenta/membresias',
    component: ClienteMembresiasComponent,
    data:{
      title:'Membresias',
      breadcrumbs:['Mi cuenta','Membresias']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'mi-cuenta/verificacion-email',
    component: NoVerificadoComponent,
    data:{
      title:'Verificación de email',
      breadcrumbs:['Mi cuenta','Verificar cuenta']
    },
    canActivate:[]
  },
  {
    path: 'pagos/pagoProveedor',
    component: ClientePagoProveedorComponent,
    data:{
      title: 'Pago Proveedor',
      breadcrumbs:['Pago Verificado', 'Deposito realizado']
    }
  },
  {
    path: 'solicitar-servicio',
    component: ClienteSolicitudServicioComponent,
    data:{
      title:'Solicitud por servicio',
      breadcrumbs:['Solicitud','Por servicio']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'servicio/postulaciones',
    component: ClientePostulacionesServicioComponent,
    data:{
      title:'Postulaciones por servicio',
      breadcrumbs:['Servicios','Postulaciones']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'servicio/postulaciones/chat',
    component: ClientePostulacionesServicioChatComponent,
    data:{
      title:'Chat',
      breadcrumbs:['Servicios','Postulaciones','Chat']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'solicitar-servicio/servicio/nuevo',
    component: NuevoClienteSolicitudServicioComponent,
    data:{
      title:'Solicitud por servicio',
      breadcrumbs:['Solicitud','Nuevo Servicio']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'solicitar-servicio/proveedor',
    component: ClienteSolicitudProveedorComponent,
    data:{
      title:'Solicitud por proveedor',
      breadcrumbs:['Solicitud','Por proveedor']
    },
    canActivate:[AuthGuardClientService]
  },
  
  {
    path: 'balance',
    component: ClienteBalanceComponent,
    data:{
      title:'Balance',
      breadcrumbs:['Perfil','Balance']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'notificaciones',
    component: ClienteNotificacionesComponent,
    data:{
      title:'Notificaciones',
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'proveedores-favoritos',
    component: ClienteProveedoresFavoritosComponent,
    data:{
      title:'Proveedores favoritos',
      breadcrumbs:['Proveedores favoritos']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'calificaciones',
    component: ClienteCalificacionesComponent,
    data:{
      title:'Calificaciones',
      breadcrumbs:['Calificaciones']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'atencion-cliente',
    component: ClienteAtencionAlClienteComponent,
    data:{
      title:'Atención al cliente',
      breadcrumbs:['Atención al cliente']
    },
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'sugerencias-comentarios',
    component: ClienteComentariosYSugerenciasComponent,
    data:{
      title:'Comentarios y sugerencias',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardClientService]
  }
  
];
