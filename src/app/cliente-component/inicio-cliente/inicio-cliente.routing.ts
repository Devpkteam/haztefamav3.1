import { Routes } from '@angular/router';

import { InicioClienteComponent } from './inicio-cliente.component';

export const InicioClienteRoutes: Routes = [{
  path: '',
  component: InicioClienteComponent
}];
