import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';
import { RouterModule } from '@angular/router';
import { InicioClienteRoutes } from './inicio-cliente.routing';
import { InicioClienteComponent } from './inicio-cliente.component';

@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(InicioClienteRoutes)
  ],
  declarations: [InicioClienteComponent]
})
export class InicioClienteModule { }
