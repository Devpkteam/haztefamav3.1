import { Component, OnInit } from '@angular/core';
import { ProveedoresFavoritosService } from '../../globals-services/proveedores-favoritos.service';
import { BorrarComponent } from '../../globals-dialogs/borrar/borrar.component';
import { MatDialog } from '@angular/material';
import { SuccessDialogComponent } from '../../globals-dialogs/success-dialog/success-dialog.component';

@Component({
  selector: 'app-cliente-proveedores-favoritos',
  templateUrl: './cliente-proveedores-favoritos.component.html',
  styleUrls: ['./cliente-proveedores-favoritos.component.scss']
})
export class ClienteProveedoresFavoritosComponent implements OnInit {
  proveedores: any;
  url: any;

  constructor(
    private proveedoresFavoritosService:ProveedoresFavoritosService,
    private dialog:MatDialog
  ) { }

  ngOnInit() {
    this.getProveedores()
  }

  getProveedores(){
    this.proveedoresFavoritosService.getProveedoresFavoritosByUserID(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
        this.proveedores = resp.proveedores
        this.url = resp.url
      }
    })
  }

  remover(id){
    console.log(id);

    
    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '350px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.proveedoresFavoritosService.deleteProveedorFavorito(id,localStorage.getItem('id')).subscribe((resp:any)=>{
          if(resp.ok){
            this.success('Favorito eliminado',`${resp.borrado.proveedor.nombre}  ${resp.borrado.proveedor.apellido} ya no esta en tu lista de favoritos`)
            this.getProveedores()
          }
        })
      }
      
    });
    
  }

  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

}
