import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ServiciosService } from '../../../globals-services/solicitud.service';
import { CategoriaService } from '../../../globals-services/categoria.service';
import { SubcategoriaService } from '../../../globals-services/subcategoria.service';
import { Router } from '@angular/router';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { MatDialog } from '@angular/material';


import { AbstractControl } from '@angular/forms'
import { AceptarComponent } from '../../../globals-dialogs/aceptar/aceptar.component';
import { ErrorDialogComponent } from '../../../globals-dialogs/error-dialog/error-dialog.component';



@Component({
  selector: 'app-nuevo-cliente-solicitud-servicio',
  templateUrl: './nuevo-cliente-solicitud-servicio.component.html',
  styleUrls: ['./nuevo-cliente-solicitud-servicio.component.scss']
})
export class NuevoClienteSolicitudServicioComponent implements OnInit {

  constructor(
    private fb:FormBuilder,
    private solicitudServices:ServiciosService,
    private categoriasServices: CategoriaService,
    private subcategoria: SubcategoriaService,
    private routin: Router,
    private dialog: MatDialog,
    private subcategoriasService:SubcategoriaService,
    private categoriaService:CategoriaService
  ) { }

  categorias = []
  subcategorias = []

  ngOnInit(): void {
    
  this.getCategorias()

  }


  hoy = Date.now()
  minDate = new Date();
  solicitudesForm = this.fb.group({
    usuario: [localStorage.getItem('id'), Validators.required],
    nombre: ['', [Validators.required]],
    descripcion: ['', [Validators.required]],
    max_price: ['', [Validators.required,Validators.pattern(/^[0-9.]+$/)]],
    min_price: ['', [Validators.required,Validators.pattern(/^[0-9.]+$/)]],
    categoria: ['', [Validators.required]],
    subcategoria: ['', [Validators.required]],
    lugar_evento: ['', [Validators.required]],
    fecha_evento: ['', [Validators.required]],
    favs: [false, [Validators.required]] 
  },{validators:this.checkvalues})

  checkvalues(form: FormGroup){
    console.log(form);
    
    let min_price = form.controls.min_price.value;
    let max_price = form.controls.max_price.value;    
  
    if(min_price > max_price){
      form.controls.min_price.setErrors({ 'repeatInvalid' : true })      
    }
    return null
  }


  crearSolicitud(){

    const dialogRef = this.dialog.open(AceptarComponent, {
      width: '470px',
      data:{msg:`Nueva solicitud`,content:`¿Seguro deseas crear una solicitud para este servicio?`},
      disableClose:true
    
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.solicitudServices.setServicios(this.solicitudesForm.value).subscribe((resp:any)=>{
          if(resp.ok){
            this.routin.navigate(['/cliente/solicitar-servicio'])
            this.success('Solicitud creada','Se ha creado una solicitud para tu servicio')
          }else{
            this.errorDialog('Algo salio mal','Su solicitud de servicio no se pudo crear')
          }
        })
      }
    })
    
    
    
  }

  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

  errorDialog(title,msg){
 
    this.dialog.open(ErrorDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }
  



 

  getCategorias(){
    this.categoriaService.getCategorias().subscribe((resp:any)=>{
      if(resp.ok){
        this.categorias = resp.categorias
      }
      
    })
  }


getSubcategorias(id){
    console.log(id);
    
    this.subcategoriasService.getSubcategoriasByCategoria(id).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.subcategorias = resp.subcategorias
      }
    })
  }

  getNameOfCategoria(id){
    let categoria = this.categorias.find((categoria)=>{
      return categoria._id == id
    })

    return categoria.nombre
    
  }
  getNameOfSubcategoria(id){
    let subcategoria = this.subcategorias.find((subcategoria)=>{
      return subcategoria._id == id
    })

    return subcategoria.nombre
    
  }

}
