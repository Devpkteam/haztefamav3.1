import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoClienteSolicitudServicioComponent } from './nuevo-cliente-solicitud-servicio.component';

describe('NuevoClienteSolicitudServicioComponent', () => {
  let component: NuevoClienteSolicitudServicioComponent;
  let fixture: ComponentFixture<NuevoClienteSolicitudServicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoClienteSolicitudServicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoClienteSolicitudServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
