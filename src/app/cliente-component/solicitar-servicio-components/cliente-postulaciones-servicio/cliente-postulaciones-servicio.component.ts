import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostulacionesService } from '../../../globals-services/postulaciones.service';
import { MatDialog } from '@angular/material';
import { ErrorDialogComponent } from '../../../globals-dialogs/error-dialog/error-dialog.component';

@Component({
  selector: 'app-cliente-postulaciones-servicio',
  templateUrl: './cliente-postulaciones-servicio.component.html',
  styleUrls: ['./cliente-postulaciones-servicio.component.scss']
})
export class ClientePostulacionesServicioComponent implements OnInit {
  id: any;
  url: any;


  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private postulacionesServices:PostulacionesService,
    private dialog:MatDialog
  ) { 

    route.queryParams.subscribe((data)=>{
      this.id = data.id
    })
  }

  
  postulaciones = []
  ngOnInit() {

    this.getPostulaciones()
  }

  getPostulaciones() {
    this.postulacionesServices.getPostulacionesPorServicio(this.id).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.postulaciones = resp.postulaciones
        this.url = resp.url
      }else{
        this.router.navigate(['/cliente/solicitar-servicio'])
        this.errorDialog('Algo ha salido mal','No se pudo encontrar el requerimiento')
      }
    })
  }


  
errorDialog(title,msg){
 
  this.dialog.open(ErrorDialogComponent, {
    width: '500px',
    data:{title,msg}
  });

}

chatear(id){
  this.router.navigate(['/cliente/servicio/postulaciones/chat'],{queryParams:{id}})
}

}
