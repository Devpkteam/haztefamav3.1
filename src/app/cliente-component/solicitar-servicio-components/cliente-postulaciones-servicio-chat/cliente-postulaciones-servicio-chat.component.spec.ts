/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ClientePostulacionesServicioChatComponent } from './cliente-postulaciones-servicio-chat.component';

describe('ClientePostulacionesServicioChatComponent', () => {
  let component: ClientePostulacionesServicioChatComponent;
  let fixture: ComponentFixture<ClientePostulacionesServicioChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientePostulacionesServicioChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientePostulacionesServicioChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
