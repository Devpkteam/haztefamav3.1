import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { AceptarComponent } from '../../../globals-dialogs/aceptar/aceptar.component';
import { Validators, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ServiciosService } from '../../../globals-services/solicitud.service';
import { UsuarioService } from '../../../globals-services/usuario.service';
import { ChatPostulacionesService } from '../../../globals-services/chat-postulaciones.service';
import { PostulacionesService } from '../../../globals-services/postulaciones.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { ImagenComponent } from '../../../globals-dialogs/imagen/imagen.component';
import { CotizacionService } from '../../../globals-services/cotizacion.service';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { ContratoService } from '../../../globals-services/contrato.service';
import { ContratoComponent } from '../../../globals-dialogs/contrato/contrato.component';
import { async } from '@angular/core/testing';
import { ErrorDialogComponent } from '../../../globals-dialogs/error-dialog/error-dialog.component';

@Component({
  selector: 'app-cliente-postulaciones-servicio-chat',
  templateUrl: './cliente-postulaciones-servicio-chat.component.html',
  styleUrls: ['./cliente-postulaciones-servicio-chat.component.scss']
})
export class ClientePostulacionesServicioChatComponent implements OnInit {

  id: any;
  mensajes: any;
  url: any;
  servicio: any;
  myId: any;
  urlDownload: any;
  usuario: any;
  cotizacion: any;
  cotizacionStatus: string;
  contratoStatus: string;


  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private postulacionesService:PostulacionesService,
    private chatPostulacionesService:ChatPostulacionesService,
    private serviciosService:ServiciosService,
    private usuarioService:UsuarioService,
    private fb:FormBuilder,
    private cdRef:ChangeDetectorRef,
    private dialog:MatDialog,
    private socket:Socket,
    private cotizacionService:CotizacionService,
    private contratoServices:ContratoService
  ) {
  
    this.route.queryParams.subscribe((data)=>{
      this.id = data.id
      this.mensajeForm.controls.postulacion.setValue(data.id)
    })

    this.myId = localStorage.getItem('id')
   }

  ngOnInit() {
    this.getCotizacion()
    this.getMensajes()
    this.getUsuarioPostulacion()
    this.socket.emit('sala_chat',{name_room:this.id})
    this.socket.on('nuevo_mensaje_r',()=>{
      console.log('Nuevo mensaje');
      
      this.getMensajes()
    })

    this.socket.on('enviar_cotizacion_r',()=>{
      this.getCotizacion()
      this.success('Nueva cotizacion','El proveedor ha enviado una cotizacion')
    })
    this.socket.on('deshacer_cotizacion_r',()=>{
      this.getCotizacion()
      this.success('Cotización eliminada','El proveedor ha eliminado la cotización')
    })
     this.socket.on('editar_cotizacion_r',()=>{
      this.getCotizacion()
      this.success('Cotización editada','El proveedor ha editado la cotización')
    })
  }


  
  getCotizacion(){
    this.cotizacionService.getCotizacionByPostulacionId(this.id).subscribe((resp:any)=>{
      if(resp.ok){
        if(resp.cotizacion == null){
          this.cotizacionStatus = 'ninguna'
          this.contratoStatus = 'ninguna'
        }else{
          switch (resp.cotizacion.contratoStatus) {
            case 0:
              this.contratoStatus = 'iniciado'
              break;
            case 1:
              this.contratoStatus = 'recibida'
              break;
            case 2:
              this.contratoStatus = 'Listo'
              break;
            default:
              break;
            }
          switch (resp.cotizacion.status) {
            case 1:
              this.cotizacionStatus = 'recibida'
              break;
            case 2:
              this.cotizacionStatus = 'aceptada'
              break;
            default:
              break;
          }
        
          this.cotizacionRecibida = true
          this.cotizacion = resp.cotizacion
        }
      }
      
    })
  }

  getUsuarioPostulacion() {
    this.postulacionesService.getUsuarioPorPostulacionId(this.id).subscribe((resp:any)=>{
      console.log(resp);
      
      this.usuario = resp.usuario
      
    })
  }

  mensajeForm = this.fb.group({
    usuario:[localStorage.getItem('id'),[Validators.required]],
    postulacion:[this.id,[Validators.required]],
    tipo:["mensaje",[Validators.required]],
    mensaje:[''],
    imagen:[''],
    documento:['']

  })

  cotizacionRecibida = false
  getInfoServicio(){
    this.postulacionesService.getServicioPorPostulacionId(this.id).subscribe((resp:any)=>{
      console.log(resp);
      
      this.servicio = resp.servicio
      
    })
  }

  openImage(url){
    console.log(`${this.url}${url}`)
    const dialogRef = this.dialog.open(ImagenComponent, {
    
      data:{url:`${this.url}${url}`}
    
    });
  }



  getMensajes() {
    this.chatPostulacionesService.getMensajesPorPostulacion(this.id).subscribe((resp:any)=>{
      this.mensajes = resp.chat
      this.url = resp.url
      this.urlDownload = resp.urlDownload
    this.cdRef.detectChanges()

    })
  }

  onResized(event){
    console.log(event);
    
  }
  messageValid() {
    if (this.mensajeForm.value.mensaje == "") {
      if (
        this.mensajeForm.value.documento == "" &&
        this.mensajeForm.value.imagen == ""
      ) {
        this.success(
          "Mensaje vacio",
          "Debe enviar un mensaje, imagen o documento"
        );
      } else {
        this.sendMessage();
      }
    } else {
      this.sendMessage();
    }
  }
  sendMessage(){
    this.chatPostulacionesService.nuevoMensajePorPostulacion(this.mensajeForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.close()

        this.mensajeForm.controls.mensaje.setValue("")
        this.mensajeForm.controls.tipo.setValue("mensaje")
        this.mensajeForm.controls.imagen.setValue("")
        this.mensajeForm.controls.documento.setValue("")
        this.socket.emit('nuevo_mensaje',{name_room:this.id})
      }
    })
    
  }

  close(){
    this.imagenUploads.pop()
    this.documentUploads.pop()
    this.mensajeForm.controls.tipo.setValue("mensaje")
  }

  imagenUploads = []
  documentUploads = []

  @ViewChild('uploader') uploader: ElementRef;
  @ViewChild('uploaderFile') uploaderFile: ElementRef;
  @ViewChild('scrollMe') scrollMe: ElementRef;
  
  
  subirImagen($event){
    this.imagenUploads.pop()
    
    let formData: FormData = new FormData();
      formData.append('image',$event.target.files[0])
      this.usuarioService.uploadImage(formData).subscribe((resp:any)=>{
        console.log(resp);
        
        if(resp.ok){
         console.log(resp);
         this.mensajeForm.controls.tipo.setValue('imagen')
         this.mensajeForm.controls.imagen.setValue(resp.name)
          this.imagenUploads.push(resp.name)
          this.uploader.nativeElement.value = ''
        }
        
      })
      
    
  }
  subirArchivo($event){
    this.documentUploads.pop()
    let formData: FormData = new FormData();
      formData.append('image',$event.target.files[0])
      
      
      this.usuarioService.uploadImage(formData).subscribe((resp:any)=>{
        
        
        if(resp.ok){
          this.mensajeForm.controls.tipo.setValue('documento')
         this.mensajeForm.controls.documento.setValue(resp.name)
          this.documentUploads.push(resp.name)
          this.uploaderFile.nativeElement.value = ''
        
         
        }
        
      })
      
    
  }

  nameOfDocument(name){
    return name.split("@")[1]
  }

  download(name){
    const dialogRef = this.dialog.open(AceptarComponent, {
      width: '470px',
      data:{msg:`Descargar archivo`,content:`¿Seguro deseas descargar este archivo?`},
      disableClose:true
    
    });
    
    dialogRef.afterClosed().subscribe(result => {
       if(result){

         window.open(`${this.urlDownload}${name}`, "_blank");
       }
    })
    
 
  }

  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }


  rechazar(id){
    this.cotizacionService.rechazarCotizacion(id).subscribe((resp:any)=>{
      if(resp.ok){
        this.getCotizacion()
        this.success('Cotizacion rechazada','Se rechazo la cotizacion')
        this.socket.emit('cotizacion_rechazada',{name_room:this.id})
      }
    })
    
  }

  pagar(id){
    console.log("que coño?");
    this.contratoServices.getContratoByDestino('client').subscribe( async (resp: any)=>{
      if (resp.ok) {
          let contrato = resp.contrato[0].contrato
          let titulo = resp.contrato[0].titulo

          console.log(contrato);
          
          const dialogRef = this.dialog.open(ContratoComponent, {
            width: '600px',
            data:{msg:`${titulo}`,content:`${contrato}`},
            disableClose:true
          
          });          
          

          let form = {contratoStatus: 1} 

          this.getInfoServicio()
          
          dialogRef.afterClosed().subscribe( async result => {
            if(result){
              this.cotizacionService.aceptarCotizacion(id, form).subscribe((resp:any)=>{
                if(resp.ok){
                  this.getCotizacion()
                  this.success('Cotizacion aceptada','Se acepto la cotizacion')
                  this.socket.emit('cotizacion_aceptada',{name_room:this.id})
                }
              })
              this.serviciosService.pagarServicios({servicios: this.servicio._id,cotizaciones:id,usuario:localStorage.getItem('id')}).subscribe((resp:any)=>{
                if(resp.ok){
                  console.log(resp.data.url);
                  this.success('Solicitud creada','Se procedera a procesar el pago')
                  window.open(resp.data.url, "_blank");
                }else{
                  this.errorDialog('Ocurrio un error','Algo ha salido mal en su solicitud, intentelo mas tarde')
                }
              })  
            }
          })
      }
    })

  }
  
  errorDialog(title,msg){
 
    this.dialog.open(ErrorDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

}
