import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ServiciosService } from '../../../globals-services/solicitud.service';

import { Socket } from "ngx-socket-io";
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { BorrarComponent } from '../../../globals-dialogs/borrar/borrar.component';
import { NuevaSolicitudComponent } from '../../cliente-dialogs/nueva-solicitud/nueva-solicitud.component';
import { EditarSolicitudComponent } from '../../cliente-dialogs/editar-solicitud/editar-solicitud.component';
import { Router } from '@angular/router';




@Component({
  selector: 'app-cliente-solicitud-servicio',
  templateUrl: './cliente-solicitud-servicio.component.html',
  styleUrls: ['./cliente-solicitud-servicio.component.scss']
})
export class ClienteSolicitudServicioComponent implements OnInit {
  [x: string]: any;
servicios:any;
  constructor(
    private serviciosService: ServiciosService,
    private dialog: MatDialog,
    private socket: Socket,
    private router: Router
  ) { }

  ngOnInit() {
    this.socket.on('solicitudes_update_r', (msg) =>{
      this.getSolicitudes()
    })
    this.getSolicitudes()
  }

  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

  editar(data){

    let dialogRef = this.dialog.open(EditarSolicitudComponent, {
      width: '500px',
      data,
      disableClose: true 
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
      this.socket.emit('comentario_update')
       this.getSolicitudes()
       this.success('Solicitud Editada','Editaste una Solicitud')
      }
      
    });
  }

  url 
  solicitudes

  getSolicitudes(){    
    this.serviciosService.getServiciosByUser(localStorage.getItem('id')).subscribe((resp: any) =>{
      console.log(resp.url);
      
      if (resp.ok){
        this.servicios = resp.solicitudes;
        console.log(resp)
        this.url = resp.url
      }
    })
  }

  delete(id){
    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '400px'
    })
    
    dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.serviciosService.deleteServicios(id, localStorage.getItem('id')).subscribe((resp: any) => {
          if(resp.ok){
            this.socket.emit('solicitudes_update')
            this.success('Solicitud Eliminada', 'Marcaste esta solicitud como eliminado')
            this.getSolicitudes()
          }
        })
      }
    })
  }

  nuevaSolicitud(){
    this.router.navigate(['/cliente/solicitar-servicio/servicio/nuevo'])

  
  }

  postulaciones(id){
    this.router.navigate(['/cliente/servicio/postulaciones'],{queryParams:{id}})
  }
}
