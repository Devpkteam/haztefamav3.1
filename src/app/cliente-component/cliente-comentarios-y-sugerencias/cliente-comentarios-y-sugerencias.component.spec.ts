/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ClienteComentariosYSugerenciasComponent } from './cliente-comentarios-y-sugerencias.component';

describe('ClienteComentariosYSugerenciasComponent', () => {
  let component: ClienteComentariosYSugerenciasComponent;
  let fixture: ComponentFixture<ClienteComentariosYSugerenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteComentariosYSugerenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteComentariosYSugerenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
