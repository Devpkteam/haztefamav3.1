import { NgModule } from '@angular/core';

import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { AdminItems } from './menu-items/admin-items';
import { ClienteItems } from './menu-items/cliente-items';
import { ProveedorItems } from './menu-items/proveedor-items';


@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective
   ],
  providers: [ MenuItems,AdminItems ,ClienteItems,ProveedorItems]
})
export class SharedModule { }
