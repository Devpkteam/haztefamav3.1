import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  group?: any[];
}


const MiCuentaGroup = [
  {state:'mi-cuenta/informacion', name: 'Información',type:'link'},
  {state:'mi-cuenta/membresias', name: 'Membresias',type:'link'},

]
const SolicitarServicioGroup = [
  {state:'solicitar-servicio/servicio', name: 'Solicitud por servicio',type:'link'},
  {state:'solicitar-servicio/proveedor', name: 'Solicitud por proveedor',type:'link'},

]



const PROVEEDORITEMS = [
  { state: 'inicio', name: 'Inicio', type: 'link', icon: 'home' },
  { state: 'busca-trabajo', name: 'Busca trabajo', type: 'link', icon: 'work'},
  { state: 'requerimientos', name: 'Mis requerimientos', type: 'link', icon: 'bookmark_border' },
  { state: 'membresias', name: 'Membresias', type: 'link', icon: 'card_membership' },
  { state: 'pagos', name: 'Pagos', type: 'link', icon: 'payments'},
  { state: 'atencion-cliente', name: 'Atencion al cliente', type: 'link', icon: 'support_agent'},
  { state: 'sugerencias-comentarios', name: 'Comentarios', type: 'link', icon: 'comment' },
 
];


@Injectable()
export class ProveedorItems {
  getMenuitem(): Menu[] {
    return PROVEEDORITEMS;
  }
}
