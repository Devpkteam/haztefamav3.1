import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  group?: any[];
  verify?: any[];
}


const MiCuentaGroup = [
  {state:'mi-cuenta/informacion', name: 'Información',type:'link'},
  {state:'mi-cuenta/membresias', name: 'Membresias',type:'link'},

]
const MiCuentaVerificado = [
  {state:'mi-cuenta/verificacion-email', name: 'Verificar cuenta',type:'link'},

]
const SolicitarServicioGroup = [
  {state:'solicitar-servicio/servicio', name: 'Solicitud por servicio',type:'link'},
  {state:'solicitar-servicio/proveedor', name: 'Solicitud por proveedor',type:'link'},

]



const CLIENTEITEMS = [
  { state: 'inicio', name: 'Inicio', type: 'link', icon: 'home' },
  { state: 'mi-cuenta', name: 'Mi cuenta', type: 'group', icon: 'account_box' ,group:MiCuentaGroup, verify:MiCuentaVerificado},
  { state: 'solicitar-servicio', name: 'Solicita un servicio', type: 'link', icon: 'add',group:SolicitarServicioGroup },
  { state: 'proveedores-favoritos', name: 'Proveedores favoritos', type: 'link', icon: 'favorite' },
  { state: 'calificaciones', name: 'Calificaciones', type: 'link', icon: 'star_border' },
  { state: 'atencion-cliente', name: 'Atencion al cliente', type: 'link', icon: 'support_agent'},
  { state: 'sugerencias-comentarios', name: 'Comentarios', type: 'link', icon: 'comment' },
 
];


@Injectable()
export class ClienteItems {
  getMenuitem(): Menu[] {
    return CLIENTEITEMS;
  }
}
