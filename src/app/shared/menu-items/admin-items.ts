import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  group?: any[];
}

const UsuariosGroup = [
  {state:'usuarios/administrativos',name:'Administrativos',type:'link'},
  {state:'usuarios/proveedores',name:'Proveedores',type:'link'},
  {state:'usuarios/clientes',name:'Clientes',type:'link'},
];

const ConfiguracionGroup = [
  {state:'configuracion/roles-permisos', name: 'Roles y Permisos',type:'link'},
  {state:'configuracion/categorias-servicio', name: 'Categorias de servicio',type:'link'},
  {state:'configuracion/membresias-credito', name: 'Membresias y creditos',type:'link'},
  {state:'configuracion/contratos', name: 'Contratos', type:'link'},
]

const serviciosClienteGroup = [
  
  {state:'servicios-cliente/atencion-cliente', name: 'Atencion al cliente',type:'link'},
  {state:'servicios-cliente/comentarios-sugerencias', name: 'Comentarios',type:'link'},
] 

const auditoriaGrup = [
  {state:'auditoria/estadisticas', name:'Estadisticas ', type:'link'},
  {state:'auditoria/reportes', name:'Reportes', type:'link'},
  {state:'auditoria/interna', name:'Auditoria interna', type:'link'}
]

const ADMINITEMS = [
  { state: 'inicio', name: 'Inicio', type: 'link', icon: 'home' },
  { state: 'servicios', name: 'Servicios', type: 'link', icon: 'star_border' },
  { state: 'conflictos', name: 'Conflictos', type: 'link', icon: 'sports_kabaddi' },
  { state: 'pagos', name: 'Pagos', type: 'link', icon: 'attach_money' },
  { state: 'configuracion', name: 'Configuración', type: 'group', icon: 'settings', group:ConfiguracionGroup },
  { state: 'usuarios', name: 'Usuarios', type: 'link', icon: 'people_alt' },
  { state: 'servicios-cliente', name: 'Servicios al cliente', type: 'group', icon: 'support_agent',group:serviciosClienteGroup },
  { state: 'auditoria', name: 'Auditoria', type: 'group', icon: 'insert_chart', group:auditoriaGrup },
 
];


@Injectable()
export class AdminItems {
  getMenuitem(): Menu[] {
    return ADMINITEMS;
  }
}
