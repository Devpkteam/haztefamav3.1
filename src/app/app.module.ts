
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LocationStrategy, PathLocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FullComponent } from './layouts/full/full.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './demo-material-module';

import { SharedModule } from './shared/shared.module';
import { SpinnerComponent } from './shared/spinner.component';
import { LoginComponent } from './login-components/login/login.component';
import { RecuperarPasswordComponent } from './login-components/recuperar-password/recuperar-password.component';
import { RegistroComponent } from './registro-components/registro/registro.component';
import { RegistroClienteComponent } from './registro-components/registro-cliente/registro-cliente.component';
import { RegistroProveedorComponent } from './registro-components/registro-proveedor/registro-proveedor.component';
import { NuevaPasswordComponent } from './login-components/nueva-password/nueva-password.component';
import { NewPasswordSuccesComponent } from './login-components/login-dialogs/new-password-succes/new-password-succes.component';
import { CredencialesInvalidasComponent } from './login-components/login-dialogs/credenciales-invalidas/credenciales-invalidas.component';

import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { FacebookWrongComponent } from './login-components/login-dialogs/facebook-wrong/facebook-wrong.component';
import { GoogleWrongComponent } from './login-components/login-dialogs/google-wrong/google-wrong.component';
import { EmailRepeatComponent } from './registro-components/registro-dialogs/email-repeat/email-repeat.component';
import { RegistroSuccessComponent } from './registro-components/registro-dialogs/registro-success/registro-success.component';
import { NoVerificadoComponent } from './login-components/no-verificado/no-verificado.component';
import { EmailResendComponent } from './login-components/login-dialogs/email-resend/email-resend.component';
import { VerificarComponent } from './login-components/verificar/verificar.component';
import { RecuperarPasswordDialogComponent } from './login-components/login-dialogs/recuperar-password-dialog/recuperar-password-dialog.component';
import { RecuperarPasswordFailedDialogComponent } from './login-components/login-dialogs/recuperar-password-failed-dialog/recuperar-password-failed-dialog.component';
import { adminFullComponent } from './admin-layout/admin.full.component';
import { adminHeaderComponent } from './admin-layout/admin-header/admin-header.component';
import { adminSidebarComponent } from './admin-layout/admin-sidebar/admin-sidebar.component';
import { adminBreadcrumbComponent } from './admin-layout/admin-breadcrumb/admin-breadcrumb.component';
import { CustomPaginator } from './CustomPaginatorConfiguration';
import { MatPaginatorIntl } from '@angular/material';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from '../environments/environment';
import { ModificarComponent } from './globals-dialogs/modificar/modificar.component';
import { clienteFullComponent } from './cliente-layout/cliente.full.component';
import { clienteHeaderComponent } from './cliente-layout/cliente-header/cliente-header.component';
import { clienteSidebarComponent } from './cliente-layout/cliente-sidebar/cliente-sidebar.component';
import { clienteBreadcrumbComponent } from './cliente-layout/cliente-breadcrumb/cliente-breadcrumb.component';
import { clienteSidebarHorizontalComponent } from './cliente-layout/cliente-sidebar-horizontal/cliente-sidebar-horizontal.component';
import { proveedorFullComponent } from './proveedor-layout/proveedor.full.component';
import { proveedorSidebarComponent } from './proveedor-layout/proveedor-sidebar/proveedor-sidebar.component';
import { proveedorBreadcrumbComponent } from './proveedor-layout/proveedor-breadcrumb/proveedor-breadcrumb.component';
import { proveedorSidebarHorizontalComponent } from './proveedor-layout/proveedor-sidebar-horizontal/proveedor-sidebar-horizontal.component';
import { proveedorHeaderComponent } from './proveedor-layout/proveedor-header/proveedor-header.component';
import { ClienteFooterComponent } from './cliente-layout/cliente-footer/cliente-footer.component';
import { AceptarComponent } from './globals-dialogs/aceptar/aceptar.component';
import { ImagenComponent } from './globals-dialogs/imagen/imagen.component';
import { InfoUserCommentComponent } from './globals-dialogs/info-user-comment/info-user-comment.component';
import { ContratoComponent } from './globals-dialogs/contrato/contrato.component';


const config: SocketIoConfig = { url: environment.URL_SOCKET, options: {} };

var firebaseConfig = {
   apiKey: "AIzaSyBv0DHCJ0JcrGwyfxNBs_TjoF1uDEM03R8",
   authDomain: "haztefama-b0100.firebaseapp.com",
   databaseURL: "https://haztefama-b0100.firebaseio.com",
   projectId: "haztefama-b0100",
   storageBucket: "haztefama-b0100.appspot.com",
   messagingSenderId: "752150376659",
   appId: "1:752150376659:web:5b27b90da8a80690c76426"
 }


@NgModule({
   declarations: [
      AppComponent,
      FullComponent,
      AppHeaderComponent,
      SpinnerComponent,
      AppSidebarComponent,
      LoginComponent,
      RecuperarPasswordComponent,
      RegistroComponent,
      RegistroClienteComponent,
      RegistroProveedorComponent,
      NuevaPasswordComponent,
      NewPasswordSuccesComponent,
      CredencialesInvalidasComponent,
      FacebookWrongComponent,
      GoogleWrongComponent,
      EmailRepeatComponent,
      RegistroSuccessComponent,
      NoVerificadoComponent,
      EmailResendComponent,
      VerificarComponent,
      RecuperarPasswordDialogComponent,
      RecuperarPasswordFailedDialogComponent,
      adminFullComponent,
      adminHeaderComponent,
      adminSidebarComponent,
      adminBreadcrumbComponent,
      ModificarComponent,
      clienteFullComponent,
      clienteHeaderComponent,
      clienteSidebarComponent,
      clienteBreadcrumbComponent,
      clienteSidebarHorizontalComponent,
      proveedorFullComponent,
      proveedorHeaderComponent,
 
      proveedorSidebarComponent,
      proveedorBreadcrumbComponent,
      proveedorSidebarHorizontalComponent,
      ClienteFooterComponent,
      AceptarComponent, 
      ImagenComponent, InfoUserCommentComponent, ContratoComponent


   ],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      DemoMaterialModule,
      FormsModule,
      FlexLayoutModule,
      HttpClientModule,
      SharedModule,
      RouterModule.forRoot(AppRoutes),
      ReactiveFormsModule,
      AngularFireModule.initializeApp(firebaseConfig),
      AngularFireAuthModule,
      SocketIoModule.forRoot(config),
      RecaptchaFormsModule,
      RecaptchaModule
   ],
   providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},
      { provide: MatPaginatorIntl, useValue: CustomPaginator() }],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule {}
