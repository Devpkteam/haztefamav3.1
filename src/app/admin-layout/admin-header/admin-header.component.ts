import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../../globals-services/usuario.service';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class adminHeaderComponent implements OnInit {
  usuario: any;

  constructor(
    private router:Router,
    private usuarioService:UsuarioService,
    private socket:Socket
  ){

  }


  ngOnInit(){
    this.getUser()
    this.socket.on('cambios_en_datos_r',(data)=>{  
      this.getUser()
    })
  }


  getUser(){
    this.usuarioService.getUser(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
        resp.data.url =  resp.url + resp.data.foto
        this.usuario = resp.data
       
      }
      
    })
  }


  salir(){
    localStorage.clear()
    this.router.navigate(['/ingresa'])
  }
}
