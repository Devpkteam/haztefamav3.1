import { Component, OnInit } from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import { CategoriaService } from '../../globals-services/categoria.service';
import { ServiciosService } from '../../globals-services/solicitud.service';
import { Router } from '@angular/router';
import { SuccessDialogComponent } from '../../globals-dialogs/success-dialog/success-dialog.component';
import { ErrorDialogComponent } from '../../globals-dialogs/error-dialog/error-dialog.component';
import { AceptarComponent } from '../../globals-dialogs/aceptar/aceptar.component';
import { MatDialog } from '@angular/material';
import { PostulacionesService } from '../../globals-services/postulaciones.service';

/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */


interface Categorias {
  _id:string;
  nombre: string;
  children?: Categorias[];
}



interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}


@Component({
  selector: 'app-proveedor-busca-trabajo',
  templateUrl: './proveedor-busca-trabajo.component.html',
  styleUrls: ['./proveedor-busca-trabajo.component.scss']
})
export class ProveedorBuscaTrabajoComponent implements OnInit {
  servicios: any;
  url: any;
  query: String = '';

  
  
  ngOnInit() {
    this.getServicios()

    this.getCategorias()
  }
  
  constructor(
    private categoriasService:CategoriaService,
    private serviciosService:ServiciosService,
    private router:Router,
    private dialog:MatDialog,
    private postulacionesService:PostulacionesService,
  ) {

  }

  getCategorias(){

    
    this.categoriasService.getCategoriasTree().subscribe((resp:any)=>{
 
      
      if(resp.ok){
        this.categorias = resp.categoriasTree
        this.dataSource.data = this.categorias;
      }
    })
  }

  
  getServicios(){
    this.serviciosService.getServiciosFilter({categorias:this.filterCategorias,subcategorias:this.filterSubcategorias,query:this.query}).subscribe((resp:any)=>{
     
      
      if(resp.ok){
        this.url = resp.url
        this.servicios = resp.solicitudes
      }
    })
  }

  information(id){
    this.router.navigate(['/proveedor/servicio/informacion'],{ queryParams: { id },queryParamsHandling: "merge" } )
    
  }

  
  onSearchChange(value){
    this.query = value
    this.getServicios()
  }

  categorias:Categorias[]


  filterCategorias = []
  filterSubcategorias = []


  categoriasFiltro(id){
   
    let id_exist = this.filterCategorias.find((catId)=>{
      return catId == id
    })


    

    if(id_exist == undefined){
      this.filterCategorias.push(id)
     

    } else{
      this.filterCategorias = this.filterCategorias.filter((catId)=>{
        return catId != id
      })
    }
    
  this.getServicios()

    
  }

  subcategoriasFiltro(id){

    let id_exist = this.filterSubcategorias.find((catId)=>{
      return catId == id
    })

    

    if(id_exist == undefined){
      this.filterSubcategorias.push(id)
    } else{
      this.filterSubcategorias = this.filterSubcategorias.filter((catId)=>{
        return catId != id
      })
    }
    
    this.getServicios()
    
  }

  private _transformer = (node: Categorias, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.nombre,
      id: node._id,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);


  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;


  postular(servicio){
   
    let creditos = Math.round( (((servicio.min_price + servicio.max_price)/2)/servicio.subcategoria.comision))

    
    const dialogRef = this.dialog.open(AceptarComponent, {
      width: '470px',
      data:{msg:`¿Seguro deseas postular a este servicio?`,content:`Se te descontaran ${creditos} creditos`},
      disableClose:true
    
    });
    
    dialogRef.afterClosed().subscribe(result => {
    
      if(result){
       this.postulacionesService.nuevaPostulacion({servicio,creditos,usuario:localStorage.getItem('id')}).subscribe((resp:any)=>{
         if(resp.ok){
            this.success('Postulación creada','Te has postulado a un servicio')
            this.router.navigate(['/proveedor/requerimientos'])
         }else{
           console.log(resp);
           switch (resp.err.code) {
             case 10:
              this.router.navigate(['/proveedor/membresias'])
              this.errorDialog('Creditos insuficientes','No posee creditos suficientes para postular a este servicio, por favor adquiera creditos')
               break;
             case 20:
              this.errorDialog('Postulación existente','Ya has postulado a este servicio')
               break;
           
             default:
               break;
           }
          
         }
         
       })
      }
      
    });
    
    
    
    
    }
    
    errorDialog(title,msg){
     
      this.dialog.open(ErrorDialogComponent, {
        width: '500px',
        data:{title,msg}
      });
    
    }
    
      
    success(title,msg){
     
      this.dialog.open(SuccessDialogComponent, {
        width: '500px',
        data:{title,msg}
      });
    
    }
}
