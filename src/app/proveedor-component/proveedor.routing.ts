import { Routes } from '@angular/router';

import { ProveedorInicioComponent } from './proveedor-inicio/proveedor-inicio.component';
import { AuthGuardProviderService } from '../guards/auth-guard-provider.service';
import { ProveedorMembresiasComponent } from './proveedor-membresias/proveedor-membresias.component';
import { ProveedorBuscaTrabajoComponent } from './proveedor-busca-trabajo/proveedor-busca-trabajo.component';
import { ProveedorComentariosComponent } from './proveedor-comentarios/proveedor-comentarios.component';
import { ProveedorMembresiasReturnComponent } from './proveedor-membresias-return/proveedor-membresias-return.component';
import { ProveedorMembresiasNotifyComponent } from './proveedor-membresias-notify/proveedor-membresias-notify.component';
import { ProveedorPagosComponent } from './proveedor-pagos/proveedor-pagos.component';
import { ProveedorPerfilComponent } from './proveedor-perfil/proveedor-perfil.component';
import { ProveedorRequerimientosComponent } from './proveedor-requerimientos/proveedor-requerimientos.component';
import { ProveedorChatComponent } from './proveedor-chat/proveedor-chat.component';
import { ProveedorServicioInformacionComponent } from './proveedor-servicio-informacion/proveedor-servicio-informacion.component';
import { ProveedorNotificacionesComponent } from './proveedor-notificaciones/proveedor-notificaciones.component';




export const ProveedorRoutes: Routes = [
  {
    path: 'inicio',
    component: ProveedorInicioComponent,
    data:{
      title:'Inicio',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'perfil',
    component: ProveedorPerfilComponent,
    data:{
      title:'Perfil',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
 
  {
    path: 'membresias',
    component: ProveedorMembresiasComponent,
    data:{
      title:'Membresias',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'notificaciones',
    component: ProveedorNotificacionesComponent,
    data:{
      title:'Notificaciones',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'requerimientos',
    component: ProveedorRequerimientosComponent,
    data:{
      title:'Requerimientos',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'servicio/informacion',
    component: ProveedorServicioInformacionComponent,
    data:{
      title:'Mas información',
      breadcrumbs:['Servicios','Información']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'requerimientos/chat',
    component: ProveedorChatComponent,
    data:{
      title:'Requerimientos',
      breadcrumbs:['Requerimientos','Chat']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'membresias/return',
    component: ProveedorMembresiasReturnComponent,
    data:{
      title:'',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'pagos',
    component: ProveedorPagosComponent,
    data:{
      title:'Historial de pagos',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'busca-trabajo',
    component: ProveedorBuscaTrabajoComponent,
    data:{
      title:'Buscar trabajo',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'sugerencias-comentarios',
    component: ProveedorComentariosComponent,
    data:{
      title:'Comentarios y sugerencias',
      breadcrumbs:['']
    },
    canActivate:[AuthGuardProviderService]
  },
 
  
];
