import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { MatDialog } from '@angular/material';
import { UsuarioService } from '../../globals-services/usuario.service';
import { ServiciosService } from '../../globals-services/solicitud.service';
import { Router } from '@angular/router';
import { SubcategoriaService } from '../../globals-services/subcategoria.service';
import { AceptarComponent } from '../../globals-dialogs/aceptar/aceptar.component';
import { ErrorDialogComponent } from '../../globals-dialogs/error-dialog/error-dialog.component';
import { PostulacionesService } from '../../globals-services/postulaciones.service';
import { SuccessDialogComponent } from '../../globals-dialogs/success-dialog/success-dialog.component';

@Component({
  selector: 'app-proveedor-inicio',
  templateUrl: './proveedor-inicio.component.html',
  styleUrls: ['./proveedor-inicio.component.scss']
})
export class ProveedorInicioComponent implements OnInit {


  usuario: any;
  servicios: any;
  url: any;

  constructor(
    private usuarioService:UsuarioService,
    private socket:Socket,
    private serviciosService:ServiciosService,
    private router:Router,
    private subcategoriasService:SubcategoriaService,
    private dialog:MatDialog,
    private postulacionesService:PostulacionesService
  ) { }

 
 
  ngOnInit() {
    this.getUser()

  }

  categoria = []
  subcategorias = []

  getUser(){
    this.usuarioService.getUser(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
       this.categoria[0] = resp.data.categoria._id
        this.subcategoriasService.getSubcategoriasByUserID(resp.data._id).subscribe((resp:any)=>{
          if(resp.ok){
            for (const subcategoria of resp.subcategorias) {
            
              
              this.subcategorias.push(subcategoria.subcategoria._id)
              
            }
          }
          this.getServicios(this.categoria,this.subcategorias)
         
          
        })
        resp.data.url =  resp.url + resp.data.foto
        this.usuario = resp.data

        
        this.setProfileFormValue(resp.url,resp.data)
       
      }
      
    })
  }



  getServicios(categorias,subcategorias){
    console.log('Buscando servicios con ',categorias,subcategorias);
    
    this.serviciosService.getServiciosFilter({categorias,subcategorias,query:''}).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.url = resp.url
        this.servicios = resp.solicitudes
      }
    })
  }


  information(id){
    this.router.navigate(['/proveedor/servicio/informacion'],{ queryParams: { id },queryParamsHandling: "merge" } )
    
  }

  




postular(servicio){
console.log(servicio);

let creditos = Math.round( (((servicio.min_price + servicio.max_price)/2)/servicio.subcategoria.comision))

const dialogRef = this.dialog.open(AceptarComponent, {
  width: '470px',
  data:{msg:`¿Seguro deseas postular a este servicio?`,content:`Se te descontaran ${creditos} creditos`},
  disableClose:true

});

dialogRef.afterClosed().subscribe(result => {

  if(result){
   this.postulacionesService.nuevaPostulacion({servicio,creditos,usuario:localStorage.getItem('id')}).subscribe((resp:any)=>{
     if(resp.ok){
        this.success('Postulación creada','Te has postulado a un servicio')
        this.router.navigate(['/proveedor/requerimientos'])
     }else{
       console.log(resp);
       switch (resp.err.code) {
         case 10:
          this.router.navigate(['/proveedor/membresias'])
          this.errorDialog('Creditos insuficientes','No posee creditos suficientes para postular a este servicio, por favor adquiera creditos')
           break;
         case 20:
          this.errorDialog('Postulación existente','Ya has postulado a este servicio')
           break;
       
         default:
           break;
       }
      
     }
     
   })
  }
  
});




}

errorDialog(title,msg){
 
  this.dialog.open(ErrorDialogComponent, {
    width: '500px',
    data:{title,msg}
  });

}

  
success(title,msg){
 
  this.dialog.open(SuccessDialogComponent, {
    width: '500px',
    data:{title,msg}
  });

}


  setProfileFormValue(url,data) {

    this.usuario.urlPerfil = url +data.foto
  }
}
