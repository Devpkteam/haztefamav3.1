import { Routes } from '@angular/router';

import { ProveedorInicioComponent } from './proveedor-inicio.component';

export const InicioProveedorRoutes: Routes = [{
  path: '',
  component: ProveedorInicioComponent
}];
