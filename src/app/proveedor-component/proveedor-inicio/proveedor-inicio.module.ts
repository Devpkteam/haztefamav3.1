import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProveedorInicioComponent } from './proveedor-inicio.component';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';
import { RouterModule } from '@angular/router';
import { InicioProveedorRoutes } from './proveedor-cliente.routing';

@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(InicioProveedorRoutes)
  ],
  declarations: [ProveedorInicioComponent]
})
export class ProveedorInicioModule { }
