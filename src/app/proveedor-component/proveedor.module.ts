import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';

import { DemoMaterialModule } from '../demo-material-module';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule, CLASS_NAME } from '@angular/flex-layout';
import { ProveedorRoutes } from './proveedor.routing';
import { ProveedorMembresiasComponent } from './proveedor-membresias/proveedor-membresias.component';
import { ProveedorBuscaTrabajoComponent } from './proveedor-busca-trabajo/proveedor-busca-trabajo.component';
import { ProveedorComentariosComponent } from './proveedor-comentarios/proveedor-comentarios.component';
import { ProveedorMembresiasReturnComponent } from './proveedor-membresias-return/proveedor-membresias-return.component';
import { ProveedorMembresiasNotifyComponent } from './proveedor-membresias-notify/proveedor-membresias-notify.component';
import { ProveedorPagosComponent } from './proveedor-pagos/proveedor-pagos.component';
import { ProveedorPerfilComponent } from './proveedor-perfil/proveedor-perfil.component';
import { ProveedorRequerimientosComponent } from './proveedor-requerimientos/proveedor-requerimientos.component';
import { ProveedorChatComponent } from './proveedor-chat/proveedor-chat.component';
import { ProveedorServicioInformacionComponent } from './proveedor-servicio-informacion/proveedor-servicio-informacion.component';
import { ProveedorNotificacionesComponent } from './proveedor-notificaciones/proveedor-notificaciones.component';


// import { ServiciosComponent } from './servicios/servicios.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProveedorRoutes),
    DemoMaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  entryComponents: [],
  declarations: [
   ProveedorMembresiasComponent,
   ProveedorBuscaTrabajoComponent,
   ProveedorComentariosComponent,
   ProveedorMembresiasReturnComponent,
   ProveedorMembresiasNotifyComponent,
   ProveedorPagosComponent,
   ProveedorPerfilComponent,
   ProveedorRequerimientosComponent,
   ProveedorChatComponent,
   ProveedorServicioInformacionComponent,
   ProveedorNotificacionesComponent
    
    
    
  ]
})
export class ProveedorComponentsModule {}
