import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { PostulacionesService } from '../../globals-services/postulaciones.service';

@Component({
  selector: 'app-proveedor-requerimientos',
  templateUrl: './proveedor-requerimientos.component.html',
  styleUrls: ['./proveedor-requerimientos.component.scss']
})
export class ProveedorRequerimientosComponent implements OnInit {
  postulaciones: any;
  url: any;

  constructor(
    private router:Router,
    private postulacionesService:PostulacionesService
  ) { }

  ngOnInit() {
    this.getMisPostulaciones()
  }

  chat(id){
    this.router.navigate(['/proveedor/requerimientos/chat'],{queryParams:{id}})
  }

  getMisPostulaciones(){
      this.postulacionesService.getMisPostulaciones(localStorage.getItem('id')).subscribe((resp:any)=>{
        if(resp.ok){
          console.log(resp);
          
          this.postulaciones = resp.postulaciones
          this.url = resp.url
          
        }
      })
  }

}
