import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiciosService } from '../../globals-services/solicitud.service';
import { SuccessDialogComponent } from '../../globals-dialogs/success-dialog/success-dialog.component';
import { ErrorDialogComponent } from '../../globals-dialogs/error-dialog/error-dialog.component';
import { MatDialog } from '@angular/material';
import { PostulacionesService } from '../../globals-services/postulaciones.service';
import { AceptarComponent } from '../../globals-dialogs/aceptar/aceptar.component';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-proveedor-servicio-informacion',
  templateUrl: './proveedor-servicio-informacion.component.html',
  styleUrls: ['./proveedor-servicio-informacion.component.scss']
})
export class ProveedorServicioInformacionComponent implements OnInit {
  url: any;

  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private servicioServices:ServiciosService,
    private dialog:MatDialog,
    private socket:Socket,
    private postulacionesService:PostulacionesService
  ) { 

    route.queryParams.subscribe((data)=>{
      console.log(data);
      servicioServices.getServiciosById(data.id).subscribe((resp:any)=>{
        if(resp.ok){
          this.servicio = resp.servicio
          this.url = resp.url
        }else{
          this.router.navigate(['/proveedor'])

        }
      })
      
    })
    

  }

  servicio

  ngOnInit() {
  }

  postular(servicio){
   
    
    let creditos = Math.round( (((servicio.min_price + servicio.max_price)/2)/servicio.subcategoria.comision))
    
    const dialogRef = this.dialog.open(AceptarComponent, {
      width: '470px',
      data:{msg:`¿Seguro deseas postular a este servicio?`,content:`Se te descontaran ${creditos} creditos`},
      disableClose:true
    
    });
    
    dialogRef.afterClosed().subscribe(result => {
    
      if(result){
       this.postulacionesService.nuevaPostulacion({servicio,creditos,usuario:localStorage.getItem('id')}).subscribe((resp:any)=>{
         if(resp.ok){
           this.socket.emit('notificacion_postulacion')
            this.success('Postulación creada','Te has postulado a un servicio')
            this.router.navigate(['/proveedor/requerimientos'])
         }else{
           console.log(resp);
           switch (resp.err.code) {
             case 10:
              this.router.navigate(['/proveedor/membresias'])
              this.errorDialog('Creditos insuficientes','No posee creditos suficientes para postular a este servicio, por favor adquiera creditos')
               break;
             case 20:
              this.errorDialog('Postulación existente','Ya has postulado a este servicio')
               break;
           
             default:
               break;
           }
          
         }
         
       })
      }
      
    });
    
    
    
    
    }
    
    errorDialog(title,msg){
     
      this.dialog.open(ErrorDialogComponent, {
        width: '500px',
        data:{title,msg}
      });
    
    }
    
      
    success(title,msg){
     
      this.dialog.open(SuccessDialogComponent, {
        width: '500px',
        data:{title,msg}
      });
    
    }

}
