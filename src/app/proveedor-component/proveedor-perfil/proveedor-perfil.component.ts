import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Socket } from 'ngx-socket-io';
import { UsuarioService } from '../../globals-services/usuario.service';
import { PasswordInvalidComponent } from '../../admin-component/auditoria-components/perfil-dialogs/password-invalid/password-invalid.component';
import { CambiosPerfilSuccessComponent } from '../../admin-component/auditoria-components/perfil-dialogs/cambios-perfil-success/cambios-perfil-success.component';
import { SubcategoriaService } from '../../globals-services/subcategoria.service';
import { CategoriaService } from '../../globals-services/categoria.service';

@Component({
  selector: 'app-proveedor-perfil',
  templateUrl: './proveedor-perfil.component.html',
  styleUrls: ['./proveedor-perfil.component.scss']
})
export class ProveedorPerfilComponent implements OnInit {

 usuario: any;
 subcategorias;
  subcategoriasCategoria: any;
  categorias: any;

  constructor(
    private usuarioService:UsuarioService,
    private fb:FormBuilder,
    private dialog:MatDialog,
    private socket:Socket,
    private subcategoriaService:SubcategoriaService,
    private categoriaService:CategoriaService
  ) { }

  perfilForm = this.fb.group({
    nombre: ['', [Validators.required,  Validators.maxLength(40) ,Validators.pattern("^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$")] ],
    apellido: ['', [Validators.required,  Validators.maxLength(40) ,Validators.pattern("^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$")] ],
    telefono:['',[Validators.required, Validators.minLength(8),  Validators.maxLength(11), Validators.pattern("[0-9]{9,11}")]],
    direccion:['',[Validators.required]],
    foto:['',[Validators.required]],
    categoria:['',[Validators.required]],
    subcategorias:['',Validators.required]
  })

  seguridadForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern("[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,}"), Validators.maxLength(50) ] ],
    username: ['', [Validators.required, Validators.maxLength(50) ] ],
    password: ['',[Validators.required,Validators.minLength(8)]],
    cambiar: [false],
    npassword: ['',[ Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}[^'\s]/) ]],
    rnpassword: [''],
  },{validator:this.checkPasswords})


  checkPasswords(form: FormGroup) { // funcion syncrona para verificar que las contraseñas coinciden
    let pass = form.controls.npassword.value;
    let confirmPass = form.controls.rnpassword.value;
    if(form.controls.cambiar.value == true){
      if(pass != '' && confirmPass != ''){
        if(pass !== confirmPass){
          form.controls.rnpassword.setErrors({ 'repeatInvalid' : true })
        }
      }else{
        form.controls.rnpassword.setErrors({ 'repeatInvalid' : true })
      }
    }else{
      form.controls.rnpassword.setErrors(null)
    }
  
    
  
    return null     
  }
 
  ngOnInit() {
    this.getUser()
    this.getCategorias()
  }

  getUser(){
    this.usuarioService.getUser(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
        resp.data.url =  resp.url + resp.data.foto
        this.usuario = resp.data
        this.seguridadForm.controls.email.setValue(resp.data.email)
        this.seguridadForm.controls.username.setValue(resp.data.username)
        
        this.setProfileFormValue(resp.url,resp.data)
       
      }
      this.getSubcategorias()
    })
  }


  getSubcategorias(){
    this.subcategoriaService.getSubcategoriasByUserID(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
        console.log(resp);
        
        this.subcategorias = resp.subcategorias
        let subcategorias = []
        for (const obj of resp.subcategorias) {
          
          subcategorias.push(obj.subcategoria._id);
          
      }
      this.perfilForm.controls.subcategorias.setValue(subcategorias)
      
      }
    })
  }

  getCategorias(){
 
    
    this.categoriaService.getCategorias().subscribe((resp:any)=>{
      console.log(resp);
      if(resp.ok){
        this.categorias = resp.categorias
        
      }
      
    })
  }


  getSubcategoriasByCategoria(id){
    console.log(id);
    
    this.subcategoriaService.getSubcategoriasByCategoria(id).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.subcategoriasCategoria = resp.subcategorias
      }
    })
  }


  subirImagen($event){
    let formData: FormData = new FormData();
      formData.append('image',$event.target.files[0])
      this.usuarioService.uploadImage(formData).subscribe((resp:any)=>{
        console.log(resp);
        
        if(resp.ok){
          this.perfilForm.controls.foto.setValue(resp.name)
          this.usuario.urlPerfil = resp.url
        }
        
      })
      
    
  }

  

  actualizarSeguridad(){
    this.usuarioService.updateSeguridadUser(localStorage.getItem('id'),this.seguridadForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.socket.emit('cambios_en_datos',{to:localStorage.getItem('id')})
        this.resetFormSeguridad()
        this.getUser()
        this.cambiosPerfilSuccess()
      }else{
        if(resp.err.code == 404){
          this.invalidPassword()
          
        }
      }
     
      
    })
    
  }

  invalidPassword() {
    const dialogRef = this.dialog.open(PasswordInvalidComponent, {
      width: '400px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
     
    });
    
  }
  resetFormSeguridad() {
    for (const control in this.seguridadForm.controls) {
      this.seguridadForm.controls[control].setValue('')
        this.seguridadForm.controls[control].setErrors(null)
        
      
    }
  }

  actualizarPerfil(){
    this.usuarioService.updatePerfilUser(localStorage.getItem('id'),this.perfilForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.socket.emit('cambios_en_datos',{to:localStorage.getItem('id')})
        this.getUser()
        this.cambiosPerfilSuccess()
      }
    })
  }


  cambiosPerfilSuccess() {
    const dialogRef = this.dialog.open(CambiosPerfilSuccessComponent, {
      width: '400px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
     
    });
    
  }


  setProfileFormValue(url,data) {
    this.perfilForm.controls.foto.setValue(data.foto) 
    this.perfilForm.controls.nombre.setValue(data.nombre) 
    this.perfilForm.controls.apellido.setValue(data.apellido) 
    this.perfilForm.controls.telefono.setValue(data.telefono) 
    this.perfilForm.controls.direccion.setValue(data.direccion) 
    if(data.categoria){
      this.perfilForm.controls.categoria.setValue(data.categoria._id) 
      this.getSubcategoriasByCategoria(data.categoria._id)
    }
    
    this.usuario.urlPerfil = url +data.foto
  }
}
