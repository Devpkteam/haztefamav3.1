import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { PostulacionesService } from "../../globals-services/postulaciones.service";
import { ChatPostulacionesService } from "../../globals-services/chat-postulaciones.service";
import { ServiciosService } from "../../globals-services/solicitud.service";
import { UsuarioService } from "../../globals-services/usuario.service";
import { FormBuilder, Validators } from "@angular/forms";
import { AceptarComponent } from "../../globals-dialogs/aceptar/aceptar.component";
import { MatDialog } from "@angular/material";
import { Socket } from "ngx-socket-io";
import { ImagenComponent } from "../../globals-dialogs/imagen/imagen.component";
import { EnviarCotizacionComponent } from "../../cliente-component/cliente-dialogs/enviar-cotizacion/enviar-cotizacion.component";
import { CotizacionService } from "../../globals-services/cotizacion.service";
import { storage } from "firebase";
import { SuccessDialogComponent } from "../../globals-dialogs/success-dialog/success-dialog.component";
import { EditarCotizacionComponent } from '../../cliente-component/cliente-dialogs/editar-cotizacion/editar-cotizacion.component';
import { ContratoComponent } from "../../globals-dialogs/contrato/contrato.component";
import { ContratoService } from "../../globals-services/contrato.service";

@Component({
  selector: "app-proveedor-chat",
  templateUrl: "./proveedor-chat.component.html",
  styleUrls: ["./proveedor-chat.component.scss"],
})
export class ProveedorChatComponent implements OnInit {
  id: any;
  mensajes: any;
  url: any;
  servicio: any;
  myId: any;
  urlDownload: any;
  cotizacion: any;
  cotizacionStatus: string;
  contratoStatus: string;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private postulacionesService: PostulacionesService,
    private chatPostulacionesService: ChatPostulacionesService,
    private serviciosService: ServiciosService,
    private usuarioService: UsuarioService,
    private fb: FormBuilder,
    private cdRef: ChangeDetectorRef,
    private dialog: MatDialog,
    private socket: Socket,
    private cotizacionService: CotizacionService,
    private contratoServices: ContratoService
  ) {
    this.route.queryParams.subscribe((data) => {
      this.id = data.id;
      this.mensajeForm.controls.postulacion.setValue(data.id);
    });

    this.myId = localStorage.getItem("id");
  }

  ngOnInit() {
    this.getCotizacion();
    this.getMensajes();
    this.getInfoServicio();
    this.socket.emit("sala_chat", { name_room: this.id });
    this.socket.on("nuevo_mensaje_r", () => {
      this.getMensajes();
    });


    this.socket.on("r_cotizacion_rechazada_r", () => {
      this.success("Cotizacion rechazada", "El cliente rechazo la cotizacion");
      this.getCotizacion();
    });
    this.socket.on("r_cotizacion_aceptada_r", () => {
      this.success("Cotizacion aceptada", "El cliente acepto la cotizacion");
      this.getCotizacion();
    });
  }

  mensajeForm = this.fb.group({
    usuario: [localStorage.getItem("id"), [Validators.required]],
    postulacion: [this.id, [Validators.required]],
    tipo: ["mensaje", [Validators.required]],
    mensaje: [""],
    imagen: [""],
    documento: [""],
  });

  cotizacionEnviada = false;
  getInfoServicio() {
    this.postulacionesService
      .getServicioPorPostulacionId(this.id)
      .subscribe((resp: any) => {
        console.log(resp);

        this.servicio = resp.servicio;
      });
  }

  success(title, msg) {
    this.dialog.open(SuccessDialogComponent, {
      width: "500px",
      data: { title, msg },
    });
  }

  getMensajes() {
    this.chatPostulacionesService
      .getMensajesPorPostulacion(this.id)
      .subscribe((resp: any) => {
        this.mensajes = resp.chat;
        this.url = resp.url;
        this.urlDownload = resp.urlDownload;
        this.cdRef.detectChanges();
      });
  }

  onResized(event) {
    console.log(event);
  }
  messageValid() {
    if (this.mensajeForm.value.mensaje == "") {
      if (
        this.mensajeForm.value.documento == "" &&
        this.mensajeForm.value.imagen == ""
      ) {
        this.success(
          "Mensaje vacio",
          "Debe enviar un mensaje, imagen o documento"
        );
      } else {
        this.sendMessage();
      }
    } else {
      this.sendMessage();
    }
  }
  sendMessage() {
    console.log(this.mensajeForm.value);
    this.chatPostulacionesService
      .nuevoMensajePorPostulacion(this.mensajeForm.value)
      .subscribe((resp: any) => {
        if (resp.ok) {
          this.close();

          this.mensajeForm.controls.mensaje.setValue("");
          this.mensajeForm.controls.tipo.setValue("mensaje");
          this.mensajeForm.controls.imagen.setValue("");
          this.mensajeForm.controls.documento.setValue("");
          this.socket.emit("nuevo_mensaje", { name_room: this.id });
        }
      });
  }

  openImage(url) {
    console.log(`${this.url}${url}`);
    const dialogRef = this.dialog.open(ImagenComponent, {
      data: { url: `${this.url}${url}` },
    });
  }

  close() {
    this.imagenUploads.pop();
    this.documentUploads.pop();
    this.mensajeForm.controls.tipo.setValue("mensaje");
  }

  imagenUploads = [];
  documentUploads = [];

  @ViewChild("uploader") uploader: ElementRef;
  @ViewChild("uploaderFile") uploaderFile: ElementRef;
  @ViewChild("scrollMe") scrollMe: ElementRef;

  subirImagen($event) {
    this.imagenUploads.pop();

    let formData: FormData = new FormData();
    formData.append("image", $event.target.files[0]);
    this.usuarioService.uploadImage(formData).subscribe((resp: any) => {
      console.log(resp);

      if (resp.ok) {
        console.log(resp);
        this.mensajeForm.controls.tipo.setValue("imagen");
        this.mensajeForm.controls.imagen.setValue(resp.name);
        this.imagenUploads.push(resp.name);
        this.uploader.nativeElement.value = "";
      }
    });
  }
  subirArchivo($event) {
    this.documentUploads.pop();
    let formData: FormData = new FormData();
    formData.append("image", $event.target.files[0]);

    this.usuarioService.uploadImage(formData).subscribe((resp: any) => {
      if (resp.ok) {
        this.mensajeForm.controls.tipo.setValue("documento");
        this.mensajeForm.controls.documento.setValue(resp.name);
        this.documentUploads.push(resp.name);
        this.uploaderFile.nativeElement.value = "";
      }
    });
  }

  nameOfDocument(name) {
    return name.split("@")[1];
  }

  download(name) {
    const dialogRef = this.dialog.open(AceptarComponent, {
      width: "470px",
      data: {
        msg: `Descargar archivo`,
        content: `¿Seguro deseas descargar este archivo?`,
      },
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        window.open(`${this.urlDownload}${name}`, "_blank");
      }
    });
  }

  enviarCotizacion() {
    let cotizacion = this.dialog.open(EnviarCotizacionComponent, {
      width: "350px",
      data: { id: this.id, servicio: this.servicio._id },
    });

    cotizacion.afterClosed().subscribe((result) => {
      if (result) {
        this.getCotizacion();
        this.socket.emit("enviar_cotizacion", { name_room: this.id });
      }
    });
  }

  getCotizacion() {
    this.cotizacionService
      .getCotizacionByPostulacionId(this.id)
      .subscribe((resp: any) => {
        if (resp.ok) {
          if (resp.cotizacion == null) {
            this.contratoStatus = "ninguna"
          } else{
            switch (resp.cotizacion.contratoStatus) {
              case 1:
                this.contratoStatus = "cliente";
                break;
              case 2:
                this.contratoStatus = "aceptado";
                break;
              default:
                break;
            }
          }
          if (resp.cotizacion == null) {
            this.cotizacionStatus = "ninguna";
          } else {
            switch (resp.cotizacion.status) {
              case 1:
                this.cotizacionStatus = "enviada";
                break;
              case 2:
                this.cotizacionStatus = "aceptada";
                break;
              default:
                break;
            }
          }
          this.cotizacion = resp.cotizacion;
        }
      });
  }
  eliminar(id){
    this.cotizacionService.rechazarCotizacion(id).subscribe((resp:any)=>{
      if(resp.ok){
        this.getCotizacion()
        this.success('Cotización eliminada','Se elimino la cotización')
        this.socket.emit('deshacer_cotizacion',{name_room:this.id})
      }
    })
  }
  editar(id,motivo,monto){
   
    let cotizacion = this.dialog.open(EditarCotizacionComponent, {
      width: "350px",
      data: { id: id,motivo:motivo, monto:monto},
    });

    cotizacion.afterClosed().subscribe((result) => {
      if (result) {
        this.getCotizacion();
        this.success('Cotización editada','Se edito la cotización')
        this.socket.emit("editar_cotizacion", { name_room: this.id });
      }
    });
  }

  aceptar(id){

    
    this.contratoServices.getContratoByDestino('provider').subscribe((resp: any)=>{
      if (resp.ok) {
          let contrato = resp.contrato[0].contrato
          let titulo = resp.contrato[0].titulo

          console.log(contrato);
          
          const dialogRef = this.dialog.open(ContratoComponent, {
            width: '600px',
            data:{msg:`${titulo}`,content:`${contrato}`},
            disableClose:true
          
          });

          let form = {contratoStatus : 2}
          
          dialogRef.afterClosed().subscribe(result => {
             if(result){
      
               this.cotizacionService.aceptarCotizacion(id, form).subscribe((resp:any)=>{
                 if(resp.ok){
                   this.getCotizacion()
                   this.success('Contrato Aceptado','Se te realizara el pago cuando este finalizado el trabajo')
                 }
               })
    
             }
          })

        
      }
    })

  }
}
