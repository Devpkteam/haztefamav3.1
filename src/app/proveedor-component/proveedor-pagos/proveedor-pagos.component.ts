import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { UsuarioService } from '../../globals-services/usuario.service';
import { Router } from '@angular/router';
import { PagosService } from '../../globals-services/pagos.service';

@Component({
  selector: 'app-proveedor-pagos',
  templateUrl: './proveedor-pagos.component.html',
  styleUrls: ['./proveedor-pagos.component.scss']
})
export class ProveedorPagosComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(
    private dialog:MatDialog,
    private pagosServices:PagosService,
    private router:Router
  ) {

    this.getHistorial()
  }

  
  listarHistorial(historial){
    this.dataSource = new MatTableDataSource(historial)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.getHistorial()
    
  }

  displayedColumns: string[] = ['accion', 'monto','fecha'];
  dataSource
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getHistorial(){
    this.pagosServices.getMyHistorial(localStorage.getItem('id')).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.listarHistorial(resp.historial)
        
      }
    })
  }






  
 


 
  

}
