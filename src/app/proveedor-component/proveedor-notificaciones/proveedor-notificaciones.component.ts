import { Component, OnInit } from '@angular/core';
import { NotificacionesService } from '../../globals-services/notificaciones.service';
import { Socket } from 'ngx-socket-io';
import { Router } from '@angular/router';

@Component({
  selector: 'app-proveedor-notificaciones',
  templateUrl: './proveedor-notificaciones.component.html',
  styleUrls: ['./proveedor-notificaciones.component.scss']
})
export class ProveedorNotificacionesComponent implements OnInit {
  nuevos: any;
  notificaciones: any;

  constructor(
    private notificacionesService:NotificacionesService,
    private socket:Socket,
    private router:Router
  ) { }

  ngOnInit() {
    this.socket.on('nueva_notificacion_r',()=>{
      this.getNotificaciones()
    })

    this.getNotificaciones()
  }

  getNotificaciones() {
    this.notificacionesService.misNotificaciones(localStorage.getItem('id')).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.notificaciones = resp.notificaciones
        this.nuevos = resp.nuevos
      }
    })
  }

  verNotificacion(notificacion){
    console.log(notificacion);
    this.notificacionesService.notificacionVista(notificacion._id).subscribe((resp:any)=>{
      this.getNotificaciones()
      this.router.navigate([notificacion.link],{queryParams:notificacion.parametros})
    })
    
  }

}
