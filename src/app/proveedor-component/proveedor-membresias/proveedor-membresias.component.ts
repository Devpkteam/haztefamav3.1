import { Component, OnInit, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
import { MembresiaService } from '../../globals-services/membresia.service';
import { AceptarComponent } from '../../globals-dialogs/aceptar/aceptar.component';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ErrorDialogComponent } from '../../globals-dialogs/error-dialog/error-dialog.component';
import { SuccessDialogComponent } from '../../globals-dialogs/success-dialog/success-dialog.component';
import { Socket } from 'ngx-socket-io';
import { Router } from '@angular/router';
@Component({
  selector: 'app-proveedor-membresias',
  templateUrl: './proveedor-membresias.component.html',
  styleUrls: ['./proveedor-membresias.component.scss']
})
export class ProveedorMembresiasComponent implements OnInit,AfterViewInit {
  membresias: any;

  constructor(
    private membresiasService:MembresiaService,
    private elementRef: ElementRef,
    private dialog:MatDialog,
    private socket:Socket,
    private router:Router
  ) { }

  ngAfterViewInit(){

 
    this.elementRef.nativeElement.parentElement.parentElement.classList.add('haztefama-color-rojo-gradient')
 }

  ngOnInit() {
    this.getMembresias()

    this.socket.on('pago_success',(data)=>{
        this.success('Pago procesado',data.msg)
        this.router.navigate(['/proveedor/inicio'])
    })

    this.socket.on('err_pago',(data)=>{
        this.errorDialog('Algo salio mal',data.msg)
    })
  }

  getMembresias(){
    this.membresiasService.getMembresias().subscribe((resp:any)=>{
      if(resp.ok){
        this.membresias = resp.membresias
      }
    })
  }

  ngOnDestroy(){
    this.elementRef.nativeElement.parentElement.parentElement.classList.remove('haztefama-color-rojo-gradient')
  }


  adquirir(id){

    const dialogRef = this.dialog.open(AceptarComponent, {
      width: '400px',
      data:{msg:'¿Seguro deseas adquirir esta membresia?'}
  
    });
  
    dialogRef.afterClosed().subscribe(result => {

      if(result){
        this.membresiasService.comprarMembresia({membresia:id,usuario:localStorage.getItem('id')}).subscribe((resp:any)=>{
          
          console.log(resp);
          if(resp.ok){
          window.open(resp.data.url, "_blank");
           this.success('Solicitud creada','Se procedera a procesar el pago')
            
         }else{
           this.errorDialog('Ocurrio un error','Algo ha salido mal en su solicitud, intentelo mas tarde')
         }
          
        })
      }
      
    });

  
   
  }

  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }


  errorDialog(title,msg){
 
    this.dialog.open(ErrorDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

}
