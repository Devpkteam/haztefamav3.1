import { Component, OnInit } from '@angular/core';
import { NuevoComentarioComponent } from '../../cliente-component/cliente-dialogs/nuevo-comentario/nuevo-comentario.component';
import { BorrarComponent } from '../../globals-dialogs/borrar/borrar.component';
import { EditarComentarioComponent } from '../../cliente-component/cliente-dialogs/editar-comentario/editar-comentario.component';
import { SuccessDialogComponent } from '../../globals-dialogs/success-dialog/success-dialog.component';
import { MatDialog } from '@angular/material';
import { Socket } from 'ngx-socket-io';
import { ComentariosService } from '../../globals-services/comentarios.service';

@Component({
  selector: 'app-proveedor-comentarios',
  templateUrl: './proveedor-comentarios.component.html',
  styleUrls: ['./proveedor-comentarios.component.scss']
})
export class ProveedorComentariosComponent implements OnInit {

  constructor(
    private comentariosService:ComentariosService,
    private dialog:MatDialog,
    private socket:Socket
  ) { }



  ngOnInit() {
    this.socket.on('comentario_update_r',(msg)=>{
      alert('co')
      this.getComentarios()
    })
    this.getComentarios()
  }

  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

  editar(data){

    let dialogRef = this.dialog.open(EditarComentarioComponent, {
      width: '500px',
      data,
      disableClose: true 
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
      this.socket.emit('comentario_update')
       this.getComentarios()
       this.success('Comentario editado','Editaste un comentario')
      }
      
    });
    
  
  }

  url
  comentarios
 

  getComentarios(){
    console.log('buscando');
    
    this.comentariosService.getComentariosByUserId(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
        this.comentarios = resp.comentarios
        this.url = resp.url
        // console.log(comentarios)
      }
    })
  }


  delete(id){
    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '350px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.comentariosService.borrarComentarioByUser(id,localStorage.getItem('id')).subscribe((resp:any)=>{
          if (resp.ok) {
            this.socket.emit('comentario_update')
            this.success('Comentario eliminado','Marcaste este comentario como inapropiado')
            this.getComentarios()
          }
        })
      }
      
    });
    
  }  

  nuevoComentario(){
    const dialogRef = this.dialog.open(NuevoComentarioComponent, {
      width: '600px',
      disableClose: true 
   
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.socket.emit('comentario_update')
        this.getComentarios()
        this.success('Nuevo comentario!','Se ha registrado tu comentario en el sistema')
      }
    });
  }

}
