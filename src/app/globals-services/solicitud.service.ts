import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  constructor(
    public http: HttpClient
  ) { }

  setServicios(body){
    return this.http.post(`${environment.URL_API}/servicios/servicio/`, body);
  }

  cambiarStatusServiciosById(body){
    return this.http.post(`${environment.URL_API}/servicios/servicio/cambiar-status`, body)
  }

  getServiciosById(id){
    return this.http.get(`${environment.URL_API}/servicios/servicio/${id}`)
  }

  getServiciosFilter(conditions){
    return this.http.post(`${environment.URL_API}/servicios/serviciofilter`,conditions)
  }

  getServicios(){
    return this.http.get(`${environment.URL_API}/servicios/servicio/`)
  }
  getServiciosByUser(id){
    return this.http.get(`${environment.URL_API}/servicios/usuario/${id}`)
  }

  deleteServicios(id, idAdmin){
    return this.http.delete(`${environment.URL_API}/servicios/servicio/${id}/${idAdmin}`)
  }

  updateServicios(id, body){
    return this.http.put(`${environment.URL_API}/servicios/servicio/${id}`, body)
  }

  pagarServicios(body){
    return this.http.post(`${environment.URL_API}/servicios/pagarProveedor`, body)
  }

}
