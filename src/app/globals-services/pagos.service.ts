import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PagosService {

constructor(
  private http:HttpClient
) { }


getHistorial(){
 return  this.http.get(`${environment.URL_API}/pagos/historial`)
}

getMyHistorial(id){
  return this.http.get(`${environment.URL_API}/pagos/historial/${id}`)
}
getHistorialId(id){
  return this.http.get(`${environment.URL_API}/pagos/historialId/${id}`)
}
toExcel(body){
  return this.http.get(`${environment.URL_API}/pagos/toExcel`, body)
}
}
