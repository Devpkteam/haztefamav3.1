import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostulacionesService {

constructor(
  private http:HttpClient
) { }

nuevaPostulacion(body){
  return this.http.post(`${environment.URL_API}/postulaciones/servicio`,body)
}

getMisPostulaciones(id){
  return this.http.get(`${environment.URL_API}/postulaciones/usuario/${id}`)
}

getPostulacionesPorServicio(id){
  return this.http.get(`${environment.URL_API}/postulaciones/servicio/${id}`)

}

getServicioPorPostulacionId(id){
  return this.http.get(`${environment.URL_API}/postulaciones/servicioporpostulacion/${id}`)
}
getUsuarioPorPostulacionId(id){
  return this.http.get(`${environment.URL_API}/postulaciones/usuarioporpostulacion/${id}`)
}
}
