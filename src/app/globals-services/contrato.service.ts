import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContratoService {

  constructor(
    private http: HttpClient
  ) { }

  setContrato(body){
    return this.http.post(`${environment.URL_API}/contrato`, body)
  }

  cambiarStatusContratoById(body){
    return this.http.post(`${environment.URL_API}/contrato/cambiar-status`, body)
  }

  getContratoByDestino(destino){    
    return this.http.get(`${environment.URL_API}/contrato/${destino}`)
  }

  getContratos(){
    return this.http.get(`${environment.URL_API}/contrato`)
  }

  deleteContrato(id, idAdmin){
    return this.http.delete(`${environment.URL_API}/contrato/${id}/${idAdmin}`)
  }

  updateContrato(id, body){
    return this.http.put(`${environment.URL_API}/contrato/${id}`, body)
  }
}
