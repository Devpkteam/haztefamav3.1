import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  constructor(public http:HttpClient) { }

  setCategoria(body){
    return this.http.post(`${environment.URL_API}/categoria`,body)
  }
  

  cambiarStatusCategoriaByID(body){
    return this.http.post(`${environment.URL_API}/categoria/cambiar-estatus`,body)
  }
  
  getCategoriaById(id){
    return this.http.get(`${environment.URL_API}/categoria/categoria/${id}`)
  
  }
  
  getCategorias(){
    return this.http.get(`${environment.URL_API}/categoria/categoria`)
  
  }
  getCategoriasTree(){
    return this.http.get(`${environment.URL_API}/categoria/categoriatree`)
  
  }
  
  deleteCategoria(id,idAdmin){
    
    return this.http.delete(`${environment.URL_API}/categoria/${id}/${idAdmin}`)
  
  }
  
  updateCategoria(id,body){
    return this.http.put(`${environment.URL_API}/categoria/${id}`,body)
  
  }

}
