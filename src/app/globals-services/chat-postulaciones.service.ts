import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChatPostulacionesService {

constructor(
  private http:HttpClient
) { }


getMensajesPorPostulacion(id){
  return this.http.get(`${environment.URL_API}/mensajespostulacion/postulacion/${id}`)
}

nuevoMensajePorPostulacion(body){
  return this.http.post(`${environment.URL_API}/mensajespostulacion/postulacion/nuevo`,body)

}

downloadDocument(name){
  return this.http.get(`${environment.URL_API}/user/documentuploads/${name}`)
}
}
