/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProveedoresFavoritosService } from './proveedores-favoritos.service';

describe('Service: ProveedoresFavoritos', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProveedoresFavoritosService]
    });
  });

  it('should ...', inject([ProveedoresFavoritosService], (service: ProveedoresFavoritosService) => {
    expect(service).toBeTruthy();
  }));
});
