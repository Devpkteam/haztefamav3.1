import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CotizacionService {

constructor(
  private http:HttpClient
) { }

crearCotizacion(body){
  return this.http.post(`${environment.URL_API}/cotizacion/cotizacion`,body)
}

getCotizacionByPostulacionId(id){
  return this.http.get(`${environment.URL_API}/cotizacion/postulacion/${id}`)

}
getCotizacionById(id){
  return this.http.get(`${environment.URL_API}/cotizacion/id/${id}`)

}
aceptarCotizacion(id, body){
  return this.http.post(`${environment.URL_API}/cotizacion/aceptar/${id}`, body)

}

rechazarCotizacion(id){
  return this.http.put(`${environment.URL_API}/cotizacion/rechazar/${id}`,{})

}
editarCotizacion(id,body){
  return this.http.put(`${environment.URL_API}/cotizacion/editar/${id}`,body)

}

}
