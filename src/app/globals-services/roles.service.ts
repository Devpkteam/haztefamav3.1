import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(
    public http: HttpClient
  ) { }

  setRole(body){
    return this.http.post(`${environment.URL_API}/roles`, body)
  }

  cambiarStatusRoleById(body){
    return this.http.post(`${environment.URL_API}/roles/cambiar-status`, body)
  }

  getRoleById(id){
    return this.http.get(`${environment.URL_API}/roles/${id}`)
  }

  getRoles(){
    return this.http.get(`${environment.URL_API}/roles`)
  }

  deleteRole(id, idAdmin){
    return this.http.delete(`${environment.URL_API}/roles/${id}/${idAdmin}`)
  }

  updateRole(id, body){
    return this.http.put(`${environment.URL_API}/roles/${id}`, body)
  }
}
