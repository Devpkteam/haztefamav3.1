/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ChatPostulacionesService } from './chat-postulaciones.service';

describe('Service: ChatPostulaciones', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatPostulacionesService]
    });
  });

  it('should ...', inject([ChatPostulacionesService], (service: ChatPostulacionesService) => {
    expect(service).toBeTruthy();
  }));
});
