import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SubcategoriaService {
  constructor(public http:HttpClient) { }

  getSubcategorias(){
    return this.http.get(`${environment.URL_API}/subcategoria`)
  }

  getSubcategoriasByCategoria(id){
    return this.http.post(`${environment.URL_API}/subcategoria/categoria`,{id})
  }
  getSubcategoriasByUserID(id){
    return this.http.post(`${environment.URL_API}/subcategoria/usuario`,{id})
  }

  createSubcategoria(body){
    return this.http.post(`${environment.URL_API}/subcategoria/`,body)
  }

  updateSubcategoria(id,body){
    return this.http.put(`${environment.URL_API}/subcategoria/${id}`,body)
  }

  cambiarStatusSubcategoriaByID(body){
    return this.http.post(`${environment.URL_API}/subcategoria/cambiar-estatus`,body)
  }

  deleteSubcategoria(id,idAdmin){
    
    return this.http.delete(`${environment.URL_API}/subcategoria/${id}/${idAdmin}`)
  
  }
  

}
