import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ComentariosService {

constructor(
  private http:HttpClient
) { }


getComentarios(pendientes){
  return this.http.get(`${environment.URL_API}/comentario/alls/${pendientes}`)
}


comentarioVisto(id,idAdmin){
  return this.http.put(`${environment.URL_API}/comentario/visto/${id}/${idAdmin}`,{})
}
borrarRespuestaComentario(id,idAdmin){
  return this.http.delete(`${environment.URL_API}/comentario/borrarRespuesta/${id}/${idAdmin}`,{})
}
borrarComentario(id,idAdmin){
  return this.http.delete(`${environment.URL_API}/comentario/borrar/${id}/${idAdmin}`,{})
}
borrarComentarioByUser(id,idUser){
  return this.http.delete(`${environment.URL_API}/comentario/borrarByUser/${id}/${idUser}`,{})
}

getComentariosByUserId(id){
  return this.http.get(`${environment.URL_API}/comentario/usuario/${id}`)
}

newComentario(body){
  return this.http.post(`${environment.URL_API}/comentario`,body)
}
respuestaComentario(id, body){
  return this.http.put(`${environment.URL_API}/comentario/respuestaComentario/${id}`, body)
}

editarComentario(id,body){
  return this.http.put(`${environment.URL_API}/comentario/editar/${id}`,body)
}

editarRespuestaComentario(id,body){
  return this.http.put(`${environment.URL_API}/comentario/editarRespuesta/${id}`,body)
}



}
