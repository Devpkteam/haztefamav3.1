import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProveedoresFavoritosService {

constructor(
  private http:HttpClient
) { }

getProveedoresFavoritosByUserID(id){
  return this.http.get(`${environment.URL_API}/proveedores/favoritos/${id}`)
}

nuevoProveedorFavorito(body){
  return this.http.post(`${environment.URL_API}/proveedores/nuevofav`,body)
}

deleteProveedorFavorito(id,idUser){
  return this.http.delete(`${environment.URL_API}/proveedores/${id}/${idUser}`)
}

}
