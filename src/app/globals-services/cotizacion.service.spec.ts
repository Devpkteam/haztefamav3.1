/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CotizacionService } from './cotizacion.service';

describe('Service: Cotizacion', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CotizacionService]
    });
  });

  it('should ...', inject([CotizacionService], (service: CotizacionService) => {
    expect(service).toBeTruthy();
  }));
});
