import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MembresiaService {

constructor(
  private http:HttpClient
) { }


getMembresias(){
  return this.http.get(`${environment.URL_API}/membresia`)
}

newMembresia(body){
  return this.http.post(`${environment.URL_API}/membresia`,body)
}

updateMembresia(id,body){
  return this.http.put(`${environment.URL_API}/membresia/update/${id}`,body)
}

updateStatusMembresia(body){
  return this.http.post(`${environment.URL_API}/membresia/cambiarStatus/`,body)
}


deleteMembresia(id,idAdmin){
    
  return this.http.delete(`${environment.URL_API}/membresia/${id}/${idAdmin}`)
}

comprarMembresia(body){
  return this.http.post(`${environment.URL_API}/membresia/comprar`,body)
}
}
