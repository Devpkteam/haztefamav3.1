import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ClienteItems } from '../../shared/menu-items/cliente-items';
import { MatMenuTrigger } from '@angular/material';
import { UsuarioService } from '../../globals-services/usuario.service';

@Component({
  selector: 'cliente-sidebar-horizontal',
  templateUrl: './cliente-sidebar-horizontal.component.html',
  styleUrls: ['./cliente-sidebar-horizontal.component.scss']
})
export class clienteSidebarHorizontalComponent implements OnDestroy,OnInit {
  mobileQuery: MediaQueryList;
  usuario:any;
  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: ClienteItems,
    public _usuarioService: UsuarioService
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    console.log(this.menuItems)
    this.usuario = {}
  }


  @ViewChild('clickHoverMenuTrigger') clickHoverMenuTrigger: MatMenuTrigger;

  

  
  logGroup(item){
    console.log(item);
    
  }
ngOnInit(){
this.getUser()
}
getUser(){
  this._usuarioService.getUser(localStorage.getItem('id')).subscribe((resp:any)=>{
    if(resp.ok){
      resp.data.url =  resp.url + resp.data.foto
      this.usuario = resp.data

     
    }
    
  })
}
  
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
