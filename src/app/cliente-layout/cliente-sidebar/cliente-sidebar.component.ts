import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ClienteItems } from '../../shared/menu-items/cliente-items';
import { UsuarioService } from '../../globals-services/usuario.service';

@Component({
  selector: 'cliente-sidebar',
  templateUrl: './cliente-sidebar.component.html',
  styleUrls: []
})
export class clienteSidebarComponent implements OnDestroy {
  mobileQuery: MediaQueryList;
  usuario:any;
  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: ClienteItems,
    public _usuarioService:UsuarioService
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.usuario = {}
  }

  ngOnInit(){
    this.getUser()
    }
    getUser(){
      this._usuarioService.getUser(localStorage.getItem('id')).subscribe((resp:any)=>{
        if(resp.ok){
          resp.data.url =  resp.url + resp.data.foto
          this.usuario = resp.data
         
         
        }
        
      })
    }

  logGroup(item){
    console.log(item);
    
  }
  
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
