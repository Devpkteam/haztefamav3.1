import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../../globals-services/usuario.service';
import { Socket } from 'ngx-socket-io';
import { NotificacionesService } from '../../globals-services/notificaciones.service';

@Component({
  selector: 'cliente-header',
  templateUrl: './cliente-header.component.html',
  styleUrls: ['./cliente-header.component.scss']
})
export class clienteHeaderComponent implements OnInit {
  usuario: any;
  notificaciones: any[];
  nuevos: any;
  
  constructor(
    private router:Router,
    private usuarioService:UsuarioService,
    private socket:Socket,
    private notificacionesService: NotificacionesService
  ){

  }


  ngOnInit(){
    this.getUser()
    this.socket.on('cambios_en_datos_r',(data)=>{  
      this.getUser()
      this.getNotificaciones()

    })

      this.getNotificaciones()

  }

  getNotificaciones() {
    this.notificacionesService.misNotificaciones(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
        this.notificaciones = resp.notificaciones
        this.nuevos = resp.nuevos
      }
    })
  }
  verNotificacion(id){
    console.log(id);
    this.router.navigate(['/cliente/servicio/postulaciones'],{queryParams:{id}})
    
  }

  getUser(){
    this.usuarioService.getUser(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
        resp.data.url =  resp.url + resp.data.foto
        this.usuario = resp.data
       
      }
      
    })
  }


  salir(){
    localStorage.clear()
    this.router.navigate(['/ingresa'])
  }
}
