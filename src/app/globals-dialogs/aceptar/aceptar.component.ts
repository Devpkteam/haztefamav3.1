import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-aceptar',
  templateUrl: './aceptar.component.html',
  styleUrls: ['./aceptar.component.scss']
})
export class AceptarComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AceptarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  
  ) { }

  ngOnInit() {
  }

 

  aceptar(){
    this.dialogRef.close(true);
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
