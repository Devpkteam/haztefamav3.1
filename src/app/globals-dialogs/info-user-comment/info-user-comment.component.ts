import { Component, OnInit,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UsuarioService } from '../../globals-services/usuario.service';

@Component({
  selector: 'app-info-user-comment',
  templateUrl: './info-user-comment.component.html',
  styleUrls: ['./info-user-comment.component.css']
})
export class InfoUserCommentComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<InfoUserCommentComponent>,
    @Inject(MAT_DIALOG_DATA) public usuario: any,
    public usuarioService:UsuarioService
  ) { }

  ngOnInit(): void {
       this.usuarioService.getUser(this.usuario._id).subscribe((resp:any)=>{
      if(resp.ok){
        resp.data.url =  resp.url + resp.data.foto
        this.usuario = resp.data
       
      }
      
    })
  
  }
  aceptar(){
    this.dialogRef.close(true);
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
