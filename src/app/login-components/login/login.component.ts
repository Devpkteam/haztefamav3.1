import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { NewPasswordSuccesComponent } from '../login-dialogs/new-password-succes/new-password-succes.component';
import { CredencialesInvalidasComponent } from '../login-dialogs/credenciales-invalidas/credenciales-invalidas.component';
import { AuthFirebaseService } from '../login-services/auth-firebase.service';
import { FacebookWrongComponent } from '../login-dialogs/facebook-wrong/facebook-wrong.component';
import { GoogleWrongComponent } from '../login-dialogs/google-wrong/google-wrong.component';
import { LoginService } from '../login-services/login.service';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { ErrorDialogComponent } from '../../globals-dialogs/error-dialog/error-dialog.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private fb:FormBuilder,
    private dialog:MatDialog,
    private authFirebaseService:AuthFirebaseService,
    private loginService:LoginService,
    private router:Router,
    private socket:Socket
  ) { }


  loginForm = this.fb.group( {

    email: ['', [Validators.required, Validators.email ] ],
    password: [ '',  [ Validators.required,Validators.minLength(8)] ],
    remember: [false]

 })


 login(){

   this.loginService.login(this.loginForm.value).subscribe((resp:any)=>{
    console.log(resp);
    
     
     if(resp.ok){
       if(resp.status){

         
           this.socket.emit('registerID',{id:resp.id})
           localStorage.setItem('remember',`${this.loginForm.value.remember}`)
           localStorage.setItem('id',resp.id)
           localStorage.setItem('role',resp.role)
             
           switch (resp.role) {
   
             case 'admin':
              this.socket.emit('logAdmin')
               
                 this.router.navigate(['/admin'])
               break;
             case 'client':
                 this.router.navigate(['/cliente'])
               break;
             case 'provider':
                 this.router.navigate(['/proveedor'])
               break;
             default:
               localStorage.clear()
               this.router.navigate(['/'])
               break;
           }
         
         
       }else{
         this.errorDialog('Usuario bloqueado','Su usuario se encuentra bloqueado ya que inflige las politicas de Haztefama. por favor, comuníquese con nosotros a través de nuestros medios de contacto')
       }
     }else{
       this.credencialesInvalidas()
     }
     
   })
 }
 errorDialog(title,msg){
 
  this.dialog.open(ErrorDialogComponent, {
    width: '500px',
    data:{title,msg}
  });

}


  credencialesInvalidas(){
 
    const dialogRef = this.dialog.open(CredencialesInvalidasComponent, {
      width: '450px',
  
    });

    dialogRef.afterClosed().subscribe(result => {
 
    });
  }

  faceboookWrong(){
 
    const dialogRef = this.dialog.open(FacebookWrongComponent, {
      width: '500px',
      data:{type:'login'}
    });

    dialogRef.afterClosed().subscribe(result => {
 
    });
  }

  googleWrong(){
 
    const dialogRef = this.dialog.open(GoogleWrongComponent, {
      width: '500px',
      data:{type:'login'}
  
    });

    dialogRef.afterClosed().subscribe(result => {
 
    });
  }

  
  ngOnInit() {
  }

  loginSocial(type){
    this.authFirebaseService.loginSocial(type)
      .then((resp:any)=>{

        this.socket.emit('registerID',{id:resp._id})
          localStorage.setItem('id',resp._id)
           localStorage.setItem('role',resp.role)
             
           switch (resp.role) {
   
             case 'admin':
             
               
                 this.router.navigate(['/admin'])
               break;
             case 'client':
                 this.router.navigate(['/cliente'])
               break;
             case 'provider':
                 this.router.navigate(['/proveedor'])
               break;
             default:
               localStorage.clear()
               this.router.navigate(['/'])
               break;
           }
       

        localStorage.setItem('remember',`true`)
      })
      .catch((err:any)=>{
        if(err.code == 404){
          switch (err.type) {
            case 'google':
              this.googleWrong()
              break;
            case 'facebook':
              this.faceboookWrong()
              break;
          
            default:
              break;
          }
          
        }
      })
  }

}
