import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public http:HttpClient) { }
  

  login(body){
    return this.http.post(`${environment.URL_API}/user/login`,body);
  }
  
  loginSocialNetwork(body){
    return this.http.post(`${environment.URL_API}/user/login-social-networks`,body);
  }
  
  
  
  authAccount(body){
    return this.http.post(`${environment.URL_API}/user/auth-email`,body);
  }
  
  resendVerifyEmail(body){
    return this.http.post(`${environment.URL_API}/user/resend-auth-email`,body);
  }
  
  
  
}
