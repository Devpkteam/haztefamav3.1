import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthFirebaseService } from '../../login-components/login-services/auth-firebase.service';
import { Router } from '@angular/router';
import { GoogleWrongComponent } from '../../login-components/login-dialogs/google-wrong/google-wrong.component';
import { MatDialog } from '@angular/material';
import { FacebookWrongComponent } from '../../login-components/login-dialogs/facebook-wrong/facebook-wrong.component';
import { RegisterService } from '../registro-servicios/register.service';
import { EmailRepeatComponent } from '../registro-dialogs/email-repeat/email-repeat.component';
import { RegistroSuccessComponent } from '../registro-dialogs/registro-success/registro-success.component';

@Component({
  selector: 'app-registro-cliente',
  templateUrl: './registro-cliente.component.html',
  styleUrls: ['./registro-cliente.component.scss']
})
export class RegistroClienteComponent implements OnInit {

  constructor(
    private fb:FormBuilder,
    private authFirebaseService:AuthFirebaseService,
    private router:Router,
    private dialog:MatDialog,
    private registroService:RegisterService
  ) { }

  ngOnInit() {
  }

  

  
  clienteForm = this.fb.group( {

    nombre: ['', [Validators.required,  Validators.maxLength(40) ,Validators.pattern("^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$")] ],
    apellido: ['', [Validators.required,  Validators.maxLength(40) ,Validators.pattern("^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$")] ],
    email: ['', [Validators.required, Validators.maxLength(50) ,Validators.pattern("[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,}")] ],  
    username: ['', [Validators.required, Validators.maxLength(50) ] ],
    password: [ '',  [ Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}[^'\s]/) ] ],
    rpassword: [ '',  [ Validators.required, Validators.minLength(8) ] ],
    telefono: [ '',  [Validators.required, Validators.minLength(8),  Validators.maxLength(11), Validators.pattern("[0-9]{9,11}") ] ],
    direccion: [ '', Validators.required ],
    recaptchaReactive :['',Validators.required]

 },{validator: this.checkPasswords })


 checkPasswords(form: FormGroup) { // funcion syncrona para verificar que las contraseñas coinciden
  let pass = form.controls.password.value;
  let confirmPass = form.controls.rpassword.value;
  if(pass !== confirmPass){
    form.controls.rpassword.setErrors({ 'repeatInvalid' : true })
  }

  return null     
}

resolved(captchaResponse: string) {
  console.log(`Resolved response token: ${captchaResponse}`);
 
}

register(){
  let cliente = this.clienteForm.value
  cliente.role = 'client'

  this.registroService.register(cliente).subscribe((resp:any)=>{
    console.log(resp);
    
    if(resp.ok){
      this.router.navigate(['/ingresa'])
      this.registroSucces()
    }else{
      if(resp.err.code == 11000){
        this.fieldRepeat(resp.index)
        
      }else{
        
      }
    }
    
  })
  
}
  registroSucces() {
    const dialogRef = this.dialog.open(RegistroSuccessComponent, {
      width: '500px'
    });
  
    dialogRef.afterClosed().subscribe(result => {
  
    });
  }

googleWrong(){
 
  const dialogRef = this.dialog.open(GoogleWrongComponent, {
    width: '500px',
    data:{type:'register'}

  });

  dialogRef.afterClosed().subscribe(result => {

  });
}

faceboookWrong(){
 
  const dialogRef = this.dialog.open(FacebookWrongComponent, {
    width: '500px',
    data:{type:'register'}
  });

  dialogRef.afterClosed().subscribe(result => {

  });
}

fieldRepeat(index){
  let data
  switch (index) {
    case 'email':
        data = {title:'Correo en uso',msg:'El correo electronico ya se encuentra registrado'}
      break;
    
    case 'username':
        data = {title:'Usuario en uso',msg:'El nombre de usuario ya se encuentra registrado'}
      break;
    
    default:
      break;
  }
  const dialogRef = this.dialog.open(EmailRepeatComponent, {
    width: '500px',
    data
  });

  dialogRef.afterClosed().subscribe(result => {

  });
}


errorDialog(title,msg){
 
  this.dialog.open(EmailRepeatComponent, {
    width: '500px',
    data:{title,msg}
  });

}



registerSocial(role,type){
  this.authFirebaseService.registerSocial(role,type).then((user:any)=>{
    localStorage.setItem('id',user._id);
    localStorage.setItem('role',user.role)
    this.router.navigate(['/'])
  }).catch((err)=>{

    console.log(err,'error en ')

    switch (err.type) {
      case 'google':
          if(err.code == 11000){
            this.googleWrong()
          }else{
            this.errorDialog('Registro incompleto','Sus datos no pudieron ser procesados, verifique su cuenta')
          }
        break;
      case 'facebook':
          if(err.code == 11000){
            this.faceboookWrong()
          }else{
            this.errorDialog('Registro incompleto','Sus datos no pudieron ser procesados, verifique su cuenta')
          }
        break;
    
      default:
        break;
    }
  })
}


}
