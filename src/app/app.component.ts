import { Component, HostListener, OnInit, OnDestroy } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,OnDestroy {
  @HostListener('window:beforeunload', ['$event'])

  
  doSomething($event) {
    if(localStorage.getItem('remember') != 'true'){
      localStorage.clear()
    }
  }

  constructor(
    private socket:Socket
  ){

  }

  ngOnDestroy(){
    this.socket.disconnect()
  }

  ngOnInit(){
    this.socket.connect()
  }
}
