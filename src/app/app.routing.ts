import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { LoginComponent } from './login-components/login/login.component';
import { RecuperarPasswordComponent } from './login-components/recuperar-password/recuperar-password.component';
import { RegistroComponent } from './registro-components/registro/registro.component';
import { RegistroClienteComponent } from './registro-components/registro-cliente/registro-cliente.component';
import { RegistroProveedorComponent } from './registro-components/registro-proveedor/registro-proveedor.component';
import { NoVerificadoComponent } from './login-components/no-verificado/no-verificado.component';
import { NuevaPasswordComponent } from './login-components/nueva-password/nueva-password.component';
import { VerificarComponent } from './login-components/verificar/verificar.component';
import { adminFullComponent } from './admin-layout/admin.full.component';
import { SessionGuardService } from './guards/session-guard.service';
import { AuthGuardAdminService } from './guards/auth-guard-admin.service';
import { clienteFullComponent } from './cliente-layout/cliente.full.component';
import { AuthGuardClientService } from './guards/auth-guard-client.service';
import { proveedorFullComponent } from './proveedor-layout/proveedor.full.component';
import { AuthGuardProviderService } from './guards/auth-guard-provider.service';


export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    canActivate:[SessionGuardService],
    
    children: [
      {
        path: '',
        redirectTo: '/ingresa',
        pathMatch: 'full'
      },
      {
        path: '',
        loadChildren:
          () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ],
 
  },
  {
    path: 'admin',
    component: adminFullComponent,
    children: [
      {
        path: '',
        redirectTo: 'inicio',
        pathMatch: 'full'
      },
      {
        path: '',
        loadChildren:
          () => import('./admin-component/admin.module').then(m => m.AdminComponentsModule)
      },
      {
        path: 'inicio',
        loadChildren: () => import('./admin-component/inicio/inicio.module').then(m => m.InicioModule)
      }
    ],
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'cliente',
    component: clienteFullComponent,
    children: [
      {
        path: '',
        redirectTo: 'inicio',
        pathMatch: 'full'
      },
      {
        path: '',
        loadChildren:
          () => import('./cliente-component/cliente.module').then(m => m.ClienteComponentsModule)
      },
      {
        path: 'inicio',
        loadChildren: () => import('./cliente-component/inicio-cliente/inicio-cliente.module').then(m => m.InicioClienteModule)
      }
    ],
    canActivate:[AuthGuardClientService]
  },
  {
    path: 'proveedor',
    component: proveedorFullComponent,
    children: [
      {
        path: '',
        redirectTo: 'inicio',
        pathMatch: 'full'
      },
      {
        path: '',
        loadChildren:
          () => import('./proveedor-component/proveedor.module').then(m => m.ProveedorComponentsModule)
      },
      {
        path: 'inicio',
        loadChildren: () => import('./proveedor-component/proveedor-inicio/proveedor-inicio.module').then(m => m.ProveedorInicioModule)
      }
    ],
    canActivate:[AuthGuardProviderService]
  },
  {
    path: 'ingresa',
    component:LoginComponent,
    canActivate:[SessionGuardService]
  },
  {
    path:'recuperar-contrasena',
    component:RecuperarPasswordComponent,
    canActivate:[SessionGuardService]
  },
  {
    path:'nueva-contrasena',
    component:NuevaPasswordComponent,
    canActivate:[SessionGuardService]
  },
  {
    path:'registrate',
    component:RegistroComponent,
    canActivate:[SessionGuardService]
    
  },
  {
    path:'registro-cliente',
    component:RegistroClienteComponent,
    canActivate:[SessionGuardService]
    
  },
  {
    path:'registro-proveedor',
    component:RegistroProveedorComponent,
    canActivate:[SessionGuardService]
    
  },
  {
    path:'no-verificado',
    component:NoVerificadoComponent
    
  },
  {
    path:'verificar',
    component:VerificarComponent
    
  }
];
