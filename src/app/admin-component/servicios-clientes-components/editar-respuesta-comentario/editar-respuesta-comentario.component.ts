import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormBuilder, Validators } from "@angular/forms";
import { ComentariosService } from "../../../globals-services/comentarios.service";
import { UsuarioService } from "../../../globals-services/usuario.service";
@Component({
  selector: "app-editar-respuesta-comentario",
  templateUrl: "./editar-respuesta-comentario.component.html",
  styleUrls: ["./editar-respuesta-comentario.component.css"],
})
export class EditarRespuestaComentarioComponent implements OnInit {
  usuario: any;
  constructor(
    public dialogRef: MatDialogRef<EditarRespuestaComentarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private comentarioService: ComentariosService,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
    this.getUser();
  }
  comentarioForm = this.fb.group({
    respuesta: [
      this.data.respuesta_comentario.respuesta,
      [Validators.required],
    ],
    user: "",
  });

  getUser() {
    this.usuarioService
      .getUser(localStorage.getItem("id"))
      .subscribe((resp: any) => {
        if (resp.ok) {
          resp.data.url = resp.url + resp.data.foto;
          this.usuario = resp.data;
        }
      });
  }
  aceptar() {
    this.comentarioForm.value.user = this.usuario;
    console.log(this.comentarioForm.value);
    this.comentarioService
      .editarRespuestaComentario(this.data._id, this.comentarioForm.value)
      .subscribe((resp: any) => {
        if (resp.ok) {
          this.dialogRef.close(true);
        }
      });
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }
}
