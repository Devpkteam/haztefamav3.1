import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarRespuestaComentarioComponent } from './editar-respuesta-comentario.component';

describe('EditarRespuestaComentarioComponent', () => {
  let component: EditarRespuestaComentarioComponent;
  let fixture: ComponentFixture<EditarRespuestaComentarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarRespuestaComentarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarRespuestaComentarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
