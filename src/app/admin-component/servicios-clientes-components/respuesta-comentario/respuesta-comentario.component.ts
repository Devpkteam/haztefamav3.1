import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { ComentariosService } from "../../../globals-services/comentarios.service";
import { FormBuilder, Validators } from "@angular/forms";
import { UsuarioService } from '../../../globals-services/usuario.service';;



@Component({
  selector: "app-respuesta-comentario",
  templateUrl: "./respuesta-comentario.component.html",
  styleUrls: ["./respuesta-comentario.component.css"],
})
export class RespuestaComentarioComponent implements OnInit {
  usuario:any;

  constructor(
    public dialogRef: MatDialogRef<RespuestaComentarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _comentarioService: ComentariosService,
    private fb: FormBuilder,
    public usuarioService:UsuarioService,

  ) { }

    


    
  

  ngOnInit(): void { 
    this.getUser()
    
  }

  getUser(){
    this.usuarioService.getUser(localStorage.getItem('id')).subscribe((resp:any)=>{
      if(resp.ok){
        resp.data.url =  resp.url + resp.data.foto
        this.usuario = resp.data
       
      }
      
    })
  }

  // tslint:disable-next-line: member-ordering
  comentarioForm = this.fb.group({
    user: "", 
    respuesta: ["", [Validators.required]],
  });

  cancelar(): void {
    console.log("cerrar");
    this.dialogRef.close(false);
  }
  aceptar() {
    this.comentarioForm.value.user = this.usuario;
    console.log(this.comentarioForm.value);
    

    this._comentarioService
      .respuestaComentario(this.data, this.comentarioForm.value)
      .subscribe((resp: any) => {
        if (resp.ok) {

          this.dialogRef.close(true);
        }
        console.log(resp);
      });
  }
}
