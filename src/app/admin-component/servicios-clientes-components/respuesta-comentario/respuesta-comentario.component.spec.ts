import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RespuestaComentarioComponent } from './respuesta-comentario.component';

describe('RespuestaComentarioComponent', () => {
  let component: RespuestaComentarioComponent;
  let fixture: ComponentFixture<RespuestaComentarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RespuestaComentarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RespuestaComentarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
