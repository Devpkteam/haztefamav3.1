import { Component, OnInit } from "@angular/core";
import { ComentariosService } from "../../../globals-services/comentarios.service";
import { MatDialog } from "@angular/material";
import { SuccessDialogComponent } from "../../../globals-dialogs/success-dialog/success-dialog.component";
import { Socket } from "ngx-socket-io";
import { BorrarComponent } from "../../../globals-dialogs/borrar/borrar.component";
import { InfoUserCommentComponent } from "../../../globals-dialogs/info-user-comment/info-user-comment.component";
import { RespuestaComentarioComponent } from "../respuesta-comentario/respuesta-comentario.component";
import { EditarRespuestaComentarioComponent } from "../editar-respuesta-comentario/editar-respuesta-comentario.component";
import { UsuarioService } from "../../../globals-services/usuario.service";

@Component({
  selector: "app-comentarios-y-sugerencias",
  templateUrl: "./comentarios-y-sugerencias.component.html",
  styleUrls: ["./comentarios-y-sugerencias.component.scss"],
})
export class ComentariosYSugerenciasComponent implements OnInit {
  url;
  comentarios: any = {};
  pendientes = false;
  usuario: any;
  constructor(
    private comentariosService: ComentariosService,
    private dialog: MatDialog,
    private socket: Socket,
    private usuarioService: UsuarioService
  ) {}

  vistos = false;

  ngOnInit() {
    this.socket.on("comentario_update_r", (msg) => {
  
      this.getComentarios();
    });

    this.usuarioService
      .getUser(localStorage.getItem("id"))
      .subscribe((resp: any) => {
        if (resp.ok) {
          this.usuario = resp.data;
        }
      });
    this.getComentarios();
  }

  success(title, msg) {
    this.dialog.open(SuccessDialogComponent, {
      width: "500px",
      data: { title, msg },
    });
  }

  check(id) {
    this.comentariosService
      .comentarioVisto(id, localStorage.getItem("id"))
      .subscribe((resp: any) => {
        if (resp.ok) {
          this.socket.emit("comentario_update");
          this.socket.emit("comentario_cliente", {
            to: resp.comentario.usuario._id,
          });
          this.success(
            "Comentario visto",
            "Marcaste este comentario como visto"
          );
          this.getComentarios();
        }
      });
  }

  getComentarios() {
    console.log("buscando");

    this.comentariosService
      .getComentarios(this.pendientes)
      .subscribe((resp: any) => {
        if (resp.ok) {
          this.comentarios = resp.comentarios;
          console.log(this.comentarios.respuesta);
          this.url = resp.url;
        }
      });
  }

  deleteRespond(id) {
    const dialogRef = this.dialog.open(BorrarComponent, {
      width: "350px",
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.comentariosService
          .borrarRespuestaComentario(id, localStorage.getItem("id"))
          .subscribe((resp: any) => {
            if (resp.ok) {
              this.socket.emit("comentario_update");
              this.socket.emit("comentario_cliente", {
                to: resp.comentario.usuario._id,
              });
              this.success(
                "Comentario eliminado",
                "Respuesta de comentario eliminada"
              );
              this.getComentarios();
            }
          });
      }
    });
  }
  delete(id) {
    const dialogRef = this.dialog.open(BorrarComponent, {
      width: "350px",
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.comentariosService
          .borrarComentario(id, localStorage.getItem("id"))
          .subscribe((resp: any) => {
            if (resp.ok) {
              this.socket.emit("comentario_update");
              this.socket.emit("comentario_cliente", {
                to: resp.comentario.usuario._id,
              });
              this.success(
                "Comentario eliminado",
                "Marcaste este comentario como inapropiado"
              );
              this.getComentarios();
            }
          });
      }
    });
  }
  replyComment(id) {
    const dialogRef = this.dialog.open(RespuestaComentarioComponent, {
      width: "600px",
      disableClose: true,
      data: id,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.socket.emit("comentario_update");
        
        this.success(
          "Comentario respondido!",
          "Haz respondido con exito el comentario"
        );
        this.getComentarios();
      }
    });
  }
  editComment(data) {
    const dialogRef = this.dialog.open(EditarRespuestaComentarioComponent, {
      width: "600px",
      disableClose: true,
      data,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.socket.emit("comentario_update");
      
        this.success("Comentario editado!", "Se ha editado el comentario");
        this.getComentarios();
      }
    });
  }
  getUserComment(user) {
    const dialogRef = this.dialog.open(InfoUserCommentComponent, {
      width: "360px",
      disableClose: true,
      data: user,
    });
  }
}
