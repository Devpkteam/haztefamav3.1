import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { UsuarioService } from '../../../globals-services/usuario.service';
import { RolesService } from '../../../globals-services/roles.service';
import { Router } from '@angular/router';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';

@Component({
  selector: 'app-asignar-roles',
  templateUrl: './asignar-roles.component.html',
  styleUrls: ['./asignar-roles.component.css']
})
export class AsignarRolesComponent implements OnInit {

  roles = []
  idAdmin = localStorage.getItem('id')

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort

  constructor(
    private dialog: MatDialog,
    private fb: FormBuilder,
    private usersService: UsuarioService,
    private rolesServices: RolesService,
    private router: Router
  ) {
    if(this.router.getCurrentNavigation().extras.state == undefined){
      this.router.navigate(['/admin/configuracion/roles-permisos'])
    }else{
      this.id = this.router.getCurrentNavigation().extras.state.id
    }
  }
  
  listUsers(roles){
    this.dataSource = new MatTableDataSource(roles)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.getUsers()
    this.getRoles()
  }
  displayedColumns: string[] = ['nombre', 'opciones'];
  dataSource
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getUsers(){
    this.usersService.getUserByRole('admin').subscribe((resp:any)=>{      

      if(resp.ok){
        this.listUsers(resp.data)
      }

    })
  }

  getRoles(){
    this.rolesServices.getRoles().subscribe((resp:any)=>{      

      if(resp.ok){

        this.roles = resp.roles

      }

    })
  }

  id(id: any) {
    throw new Error("Method not implemented.");
  }

  success(title, msg){

    this.dialog.open(SuccessDialogComponent, {
      width: '600px',
      data: {title, msg}
    })

  }

  asignarRol(id){
    this.usersService.updateRolAdminById(id, {roleAdmin: this.id, idAdmin: this.idAdmin}).subscribe((resp: any) => {
      if(resp.ok){
                        
        this.rolesServices.updateRole(this.id, {user_Id: id, idAdmin: this.idAdmin}).subscribe((resp: any) => {
          if (resp.ok) {

            this.roles.forEach(rol => {
              
              if (rol._id === this.id) {
                this.success('Rol modificado', `Se a añadido el rol ${rol.nombre}`)
              }
              
            });
            this.getUsers()

          }
        })
      }
    })
  }

  quitarRol(id){
    
    const unsetUser = { $unset: { roleAdmin: this.id }, idAdmin: this.idAdmin }
    const unsetRol = { $unset: { user_Id: this.id }, idAdmin: this.idAdmin }

    this.usersService.updateRolAdminById(id, unsetUser).subscribe((resp: any) => {
      if(resp.ok){
                        
        this.rolesServices.updateRole(this.id, unsetRol).subscribe((resp: any) => {
          if (resp.ok) {

            this.roles.forEach(rol => {
              
              if (rol._id === this.id) {
                this.success('Rol modificado', `Se a Elimino el rol ${rol.nombre}`)
              }
              
            });
            this.getUsers()

          }
        })
      }
    })

  }

}
