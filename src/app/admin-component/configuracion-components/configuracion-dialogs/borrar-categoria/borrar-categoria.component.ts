import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
@Component({
  selector: 'app-borrar-categoria',
  templateUrl: './borrar-categoria.component.html',
  styleUrls: ['./borrar-categoria.component.scss']
})
export class BorrarCategoriaComponent implements OnInit {

 constructor(
    public dialogRef: MatDialogRef<BorrarCategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  
  ) { }

  ngOnInit() {
  }

 

  aceptar(){
    this.dialogRef.close(true);
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
