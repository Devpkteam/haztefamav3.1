import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { MembresiaService } from '../../../../globals-services/membresia.service';

@Component({
  selector: 'app-editar-membresia',
  templateUrl: './editar-membresia.component.html',
  styleUrls: ['./editar-membresia.component.scss']
})
export class EditarMembresiaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditarMembresiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private membresiaService:MembresiaService
  ) { }

  ngOnInit() {
  }

  membresiaForm = this.fb.group({
    nombre:[this.data.nombre,[Validators.required]],
    idAdmin:[localStorage.getItem('id'),[Validators.required]],
    precio:[this.data.precio,[Validators.required,Validators.pattern(/^[0-9.]+$/)]],
    descripcion:[this.data.descripcion,[Validators.required]],
    creditos:[this.data.creditos,[Validators.required,Validators.pattern(/^[0-9.]+$/)]],
  })


  aceptar(){
   
    this.membresiaService.updateMembresia(this.data._id,this.membresiaForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true);
      }
    })
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
