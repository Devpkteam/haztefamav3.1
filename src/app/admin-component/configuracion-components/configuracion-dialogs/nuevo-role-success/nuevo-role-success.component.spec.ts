import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoRoleSuccessComponent } from './nuevo-role-success.component';

describe('NuevoRoleSuccessComponent', () => {
  let component: NuevoRoleSuccessComponent;
  let fixture: ComponentFixture<NuevoRoleSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoRoleSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoRoleSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
