import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-nuevo-role-success',
  templateUrl: './nuevo-role-success.component.html',
  styleUrls: ['./nuevo-role-success.component.css']
})
export class NuevoRoleSuccessComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevoRoleSuccessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
