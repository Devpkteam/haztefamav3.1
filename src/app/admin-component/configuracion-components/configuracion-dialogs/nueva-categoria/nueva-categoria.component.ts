import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoriaService } from '../../../../globals-services/categoria.service';

@Component({
  selector: 'app-nueva-categoria',
  templateUrl: './nueva-categoria.component.html',
  styleUrls: ['./nueva-categoria.component.scss']
})
export class NuevaCategoriaComponent implements OnInit {
constructor(
    public dialogRef: MatDialogRef<NuevaCategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private categoriasService:CategoriaService
  ) { }

  ngOnInit() {
  }

  categoriaForm = this.fb.group({
    nombre:['',[Validators.required]],
    idAdmin:[localStorage.getItem('id'),[Validators.required]]
  })


  aceptar(){
   
    this.categoriasService.setCategoria(this.categoriaForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true);
      }
    })
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
