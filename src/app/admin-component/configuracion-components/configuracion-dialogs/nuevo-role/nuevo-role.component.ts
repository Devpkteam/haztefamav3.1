import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { RolesService } from '../../../../globals-services/roles.service';

@Component({
  selector: 'app-nuevo-role',
  templateUrl: './nuevo-role.component.html',
  styleUrls: ['./nuevo-role.component.css']
})
export class NuevoRoleComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevoRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private rolesService: RolesService
  ) { }

  ngOnInit(): void {
  }

  rolForm = this.fb.group({
    nombre: ['', [Validators.required]],
    descripcion: ['', [Validators.required]],
    status: [true, [Validators.required]],
    CREATE_servicios: [false, [Validators.required]],
    READ_servicios: [false, [Validators.required]],
    UPDATE_servicios: [false, [Validators.required]],
    DELETE_servicios: [false, [Validators.required]],
    CREATE_conflictos: [false, [Validators.required]],
    READ_conflictos: [false, [Validators.required]],
    UPDATE_conflictos: [false, [Validators.required]],
    DELETE_conflictos: [false, [Validators.required]],
    CREATE_pagos: [false, [Validators.required]],
    READ_pagos: [false, [Validators.required]],
    UPDATE_pagos: [false, [Validators.required]],
    DELETE_pagos: [false, [Validators.required]],
    CREATE_rolesypermisos: [false, [Validators.required]],
    READ_rolesypermisos: [false, [Validators.required]],
    UPDATE_rolesypermisos: [false, [Validators.required]],
    DELETE_rolesypermisos: [false, [Validators.required]],
    CREATE_categorias: [false, [Validators.required]],
    READ_categorias: [false, [Validators.required]],
    UPDATE_categorias: [false, [Validators.required]],
    DELETE_categorias: [false, [Validators.required]],
    CREATE_membresia: [false, [Validators.required]],
    READ_membresia: [false, [Validators.required]],
    UPDATE_membresia: [false, [Validators.required]],
    DELETE_membresia: [false, [Validators.required]],
    CREATE_usuarios: [false, [Validators.required]],
    READ_usuarios: [false, [Validators.required]],
    UPDATE_usuarios: [false, [Validators.required]],
    DELETE_usuarios: [false, [Validators.required]],
    CREATE_atencionCliente: [false, [Validators.required]],
    READ_atencionCliente: [false, [Validators.required]],
    UPDATE_atencionCliente: [false, [Validators.required]],
    DELETE_atencionCliente: [false, [Validators.required]],
    CREATE_comentarios: [false, [Validators.required]],
    READ_comentarios: [false, [Validators.required]],
    UPDATE_comentarios: [false, [Validators.required]],
    DELETE_comentarios: [false, [Validators.required]],
    CREATE_auditoria: [false, [Validators.required]],
    READ_auditoria: [false, [Validators.required]],
    UPDATE_auditoria: [false, [Validators.required]],
    DELETE_auditoria: [false, [Validators.required]],
    idAdmin: [localStorage.getItem('id'), [Validators.required]]
  });

  aceptar(){

    this.rolesService.setRole(this.rolForm.value).subscribe((resp: any) => {
      console.log(resp);
      
      if(resp.ok){

        this.dialogRef.close(true);
      }

    })

  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
