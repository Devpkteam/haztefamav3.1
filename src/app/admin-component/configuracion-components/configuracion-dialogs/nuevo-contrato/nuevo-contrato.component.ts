import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NuevoRoleComponent } from '../nuevo-role/nuevo-role.component';
import { FormBuilder, Validators } from '@angular/forms';
import { ContratoService } from '../../../../globals-services/contrato.service';

@Component({
  selector: 'app-nuevo-contrato',
  templateUrl: './nuevo-contrato.component.html',
  styleUrls: ['./nuevo-contrato.component.css']
})
export class NuevoContratoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevoRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private contratoServicio: ContratoService
  ) { }

  ngOnInit(): void {
  }

  ContratoForm = this.fb.group({
    titulo:['',[Validators.required]],
    contrato:['',[Validators.required]],
    idAdmin:[localStorage.getItem('id'),[Validators.required]],
    destino:['',[Validators.required]],
    description:['',[Validators.required]],
  })

  aceptar(){

    this.contratoServicio.setContrato(this.ContratoForm.value).subscribe((resp: any) => {
      console.log(resp);
      
      if(resp.ok){

        this.dialogRef.close(true);
      }

    })

  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
