import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { SubcategoriaService } from '../../../../globals-services/subcategoria.service';


@Component({
  selector: 'app-nueva-subcategoria',
  templateUrl: './nueva-subcategoria.component.html',
  styleUrls: ['./nueva-subcategoria.component.scss']
})
export class NuevaSubcategoriaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevaSubcategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private subcategoriaService:SubcategoriaService
  ) { }

  ngOnInit() {
  }

  subcategoriaForm = this.fb.group({
    nombre:['',[Validators.required]],
    comision:['',[Validators.required,Validators.pattern(/^[0-9.]+$/)]],
    descripcion:['',[Validators.required]],
    categoria:[this.data.categoria,[Validators.required]],
    creditos:[0,[Validators.required,Validators.pattern(/^[0-9.]+$/)]],
    idAdmin:[localStorage.getItem('id'),[Validators.required]]
  })


  aceptar(){
    this.subcategoriaService.createSubcategoria(this.subcategoriaForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true);
      }
    })
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }
}
