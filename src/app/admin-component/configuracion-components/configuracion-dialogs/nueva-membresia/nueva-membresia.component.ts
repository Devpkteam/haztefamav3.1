import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { MembresiaService } from '../../../../globals-services/membresia.service';


@Component({
  selector: 'app-nueva-membresia',
  templateUrl: './nueva-membresia.component.html',
  styleUrls: ['./nueva-membresia.component.scss']
})
export class NuevaMembresiaComponent implements OnInit {
constructor(
    public dialogRef: MatDialogRef<NuevaMembresiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private membresiaService:MembresiaService
  ) { }

  ngOnInit() {
  }

  membresiaForm = this.fb.group({
    nombre:['',[Validators.required]],
    idAdmin:[localStorage.getItem('id'),[Validators.required]],
    precio:['',[Validators.required,Validators.pattern(/^[0-9.]+$/)]],
    descripcion:['',[Validators.required]],
    creditos:['',[Validators.required,Validators.pattern(/^[0-9.]+$/)]],
  })


  aceptar(){
   
    this.membresiaService.newMembresia(this.membresiaForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true);
      }
    })
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
