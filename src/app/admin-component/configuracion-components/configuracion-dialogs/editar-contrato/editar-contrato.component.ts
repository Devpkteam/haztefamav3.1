import { Component, OnInit, Inject } from '@angular/core';
import { ContratoService } from '../../../../globals-services/contrato.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { NuevoRoleComponent } from '../nuevo-role/nuevo-role.component';

@Component({
  selector: 'app-editar-contrato',
  templateUrl: './editar-contrato.component.html',
  styleUrls: ['./editar-contrato.component.css']
})
export class EditarContratoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevoRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private contratoServicio: ContratoService
  ) { }

  ngOnInit(): void {
  }

  ContratoForm = this.fb.group({
    titulo:[this.data.titulo,[Validators.required]],
    contrato:[this.data.contrato,[Validators.required]],
    destino:[this.data.destino,[Validators.required]],
    description:[this.data.description,[Validators.required]],
    idAdmin:[localStorage.getItem('id'),[Validators.required]],
  })

  aceptar(){

    this.contratoServicio.updateContrato(this.data._id, this.ContratoForm.value).subscribe((resp:any) => {

      if(resp.ok){
        this.dialogRef.close(true);
      }

    })
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
