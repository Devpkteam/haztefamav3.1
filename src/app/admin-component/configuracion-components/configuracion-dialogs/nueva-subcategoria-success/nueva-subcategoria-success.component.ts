import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-nueva-subcategoria-success',
  templateUrl: './nueva-subcategoria-success.component.html',
  styleUrls: ['./nueva-subcategoria-success.component.scss']
})
export class NuevaSubcategoriaSuccessComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<NuevaSubcategoriaSuccessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  
  onNoClick(): void {
    this.dialogRef.close();
  }
}
