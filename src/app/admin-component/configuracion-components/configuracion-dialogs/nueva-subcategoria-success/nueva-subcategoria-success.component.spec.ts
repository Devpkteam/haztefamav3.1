/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { NuevaSubcategoriaSuccessComponent } from './nueva-subcategoria-success.component';

describe('NuevaSubcategoriaSuccessComponent', () => {
  let component: NuevaSubcategoriaSuccessComponent;
  let fixture: ComponentFixture<NuevaSubcategoriaSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevaSubcategoriaSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaSubcategoriaSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
