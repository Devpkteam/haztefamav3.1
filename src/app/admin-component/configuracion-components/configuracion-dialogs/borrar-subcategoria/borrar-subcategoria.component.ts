import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-borrar-subcategoria',
  templateUrl: './borrar-subcategoria.component.html',
  styleUrls: ['./borrar-subcategoria.component.scss']
})
export class BorrarSubcategoriaComponent implements OnInit {
 constructor(
    public dialogRef: MatDialogRef<BorrarSubcategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  
  ) { }

  ngOnInit() {
  }

 

  aceptar(){
    this.dialogRef.close(true);
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
