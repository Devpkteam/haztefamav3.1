import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoriaService } from '../../../../globals-services/categoria.service';

@Component({
  selector: 'app-editar-categoria',
  templateUrl: './editar-categoria.component.html',
  styleUrls: ['./editar-categoria.component.scss']
})
export class EditarCategoriaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditarCategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private categoriasService:CategoriaService
  ) { }

  ngOnInit() {
  }

  editCategoriaForm = this.fb.group({
    nombre:[this.data.nombre,[Validators.required]],
    id:[this.data._id,[Validators.required]],
    idAdmin:[localStorage.getItem('id'),[Validators.required]]
  })


  aceptar(){
    let id = this.editCategoriaForm.value.id
    delete this.editCategoriaForm.value.id
    this.categoriasService.updateCategoria(id,this.editCategoriaForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true)
      }
    })
 
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
