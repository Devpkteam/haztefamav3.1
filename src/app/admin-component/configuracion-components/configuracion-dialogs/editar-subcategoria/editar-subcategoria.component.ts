import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { SubcategoriaService } from '../../../../globals-services/subcategoria.service';
@Component({
  selector: 'app-editar-subcategoria',
  templateUrl: './editar-subcategoria.component.html',
  styleUrls: ['./editar-subcategoria.component.scss']
})
export class EditarSubcategoriaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditarSubcategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private subcategoriaService:SubcategoriaService
  ) { }

  ngOnInit() {
  }

  editarSubcategoriaForm = this.fb.group({
    id:[this.data._id,[Validators.required]],
    nombre:[this.data.nombre,[Validators.required]],
    comision:[this.data.comision,[Validators.required,Validators.pattern(/^[0-9.]+$/)]],
    creditos:[this.data.creditos,[Validators.required,Validators.pattern(/^[0-9.]+$/)]],
    descripcion:[this.data.descripcion,[Validators.required]],
    categoria:[this.data.categoria,[Validators.required]],
    idAdmin:[localStorage.getItem('id'),[Validators.required]]
  })


  aceptar(){
    let id = this.editarSubcategoriaForm.value.id
    delete this.editarSubcategoriaForm.value.id
    this.subcategoriaService.updateSubcategoria(id,this.editarSubcategoriaForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true)
      }
    })
    
  }
  
  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
