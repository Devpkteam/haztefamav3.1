import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { RolesService } from '../../../../globals-services/roles.service';

@Component({
  selector: 'app-editar-role',
  templateUrl: './editar-role.component.html',
  styleUrls: ['./editar-role.component.css']
})
export class EditarRoleComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditarRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private rolesService: RolesService
  ) { }

  ngOnInit(): void {
  }

  rolForm = this.fb.group({
    nombre: [this.data.nombre, [Validators.required]],
    descripcion: [this.data.descripcion, [Validators.required]],
    status: [this.data.status, [Validators.required]],
    CREATE_servicios: [this.data.CREATE_servicios, [Validators.required]],
    READ_servicios: [this.data.READ_servicios, [Validators.required]],
    UPDATE_servicios: [this.data.UPDATE_servicios, [Validators.required]],
    DELETE_servicios: [this.data.DELETE_servicios, [Validators.required]],
    CREATE_conflictos: [this.data.CREATE_conflictos, [Validators.required]],
    READ_conflictos: [this.data.READ_conflictos, [Validators.required]],
    UPDATE_conflictos: [this.data.UPDATE_conflictos, [Validators.required]],
    DELETE_conflictos: [this.data.DELETE_conflictos, [Validators.required]],
    CREATE_pagos: [this.data.CREATE_pagos, [Validators.required]],
    READ_pagos: [this.data.READ_pagos, [Validators.required]],
    UPDATE_pagos: [this.data.UPDATE_pagos, [Validators.required]],
    DELETE_pagos: [this.data.DELETE_pagos, [Validators.required]],
    CREATE_rolesypermisos: [this.data.CREATE_rolesypermisos, [Validators.required]],
    READ_rolesypermisos: [this.data.READ_rolesypermisos, [Validators.required]],
    UPDATE_rolesypermisos: [this.data.UPDATE_rolesypermisos, [Validators.required]],
    DELETE_rolesypermisos: [this.data.DELETE_rolesypermisos, [Validators.required]],
    CREATE_categorias: [this.data.CREATE_categorias, [Validators.required]],
    READ_categorias: [this.data.READ_categorias, [Validators.required]],
    UPDATE_categorias: [this.data.UPDATE_categorias, [Validators.required]],
    DELETE_categorias: [this.data.DELETE_categorias, [Validators.required]],
    CREATE_membresia: [this.data.CREATE_membresia, [Validators.required]],
    READ_membresia: [this.data.READ_membresia, [Validators.required]],
    UPDATE_membresia: [this.data.UPDATE_membresia, [Validators.required]],
    DELETE_membresia: [this.data.DELETE_membresia, [Validators.required]],
    CREATE_usuarios: [this.data.CREATE_usuarios, [Validators.required]],
    READ_usuarios: [this.data.READ_usuarios, [Validators.required]],
    UPDATE_usuarios: [this.data.UPDATE_usuarios, [Validators.required]],
    DELETE_usuarios: [this.data.DELETE_usuarios, [Validators.required]],
    CREATE_atencionCliente: [this.data.CREATE_atencionCliente, [Validators.required]],
    READ_atencionCliente: [this.data.READ_atencionCliente, [Validators.required]],
    UPDATE_atencionCliente: [this.data.UPDATE_atencionCliente, [Validators.required]],
    DELETE_atencionCliente: [this.data.DELETE_atencionCliente, [Validators.required]],
    CREATE_comentarios: [this.data.CREATE_comentarios, [Validators.required]],
    READ_comentarios: [this.data.READ_comentarios, [Validators.required]],
    UPDATE_comentarios: [this.data.UPDATE_comentarios, [Validators.required]],
    DELETE_comentarios: [this.data.DELETE_comentarios, [Validators.required]],
    CREATE_auditoria: [this.data.CREATE_auditoria, [Validators.required]],
    READ_auditoria: [this.data.READ_auditoria, [Validators.required]],
    UPDATE_auditoria: [this.data.UPDATE_auditoria, [Validators.required]],
    DELETE_auditoria: [this.data.DELETE_auditoria, [Validators.required]],
    idAdmin: [localStorage.getItem('id'), [Validators.required]]
  });

  aceptar(){

    this.rolesService.updateRole(this.data._id, this.rolForm.value).subscribe((resp:any) => {

      if(resp.ok){
        this.dialogRef.close(true);
      }

    })
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
