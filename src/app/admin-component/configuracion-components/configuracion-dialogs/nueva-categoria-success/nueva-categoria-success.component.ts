import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-nueva-categoria-success',
  templateUrl: './nueva-categoria-success.component.html',
  styleUrls: ['./nueva-categoria-success.component.scss']
})
export class NuevaCategoriaSuccessComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevaCategoriaSuccessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  
  onNoClick(): void {
    this.dialogRef.close();
  }


}
