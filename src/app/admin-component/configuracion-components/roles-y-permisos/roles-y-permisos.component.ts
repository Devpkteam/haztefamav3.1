import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { RolesService } from '../../../globals-services/roles.service';
import { Router } from '@angular/router';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { NuevoRoleComponent } from '../configuracion-dialogs/nuevo-role/nuevo-role.component';
import { NuevoRoleSuccessComponent } from '../configuracion-dialogs/nuevo-role-success/nuevo-role-success.component';
import { BorrarComponent } from '../../../globals-dialogs/borrar/borrar.component';
import { EditarRoleComponent } from '../configuracion-dialogs/editar-role/editar-role.component';

@Component({
  selector: 'app-roles-y-permisos',
  templateUrl: './roles-y-permisos.component.html',
  styleUrls: ['./roles-y-permisos.component.scss']
})
export class RolesYPermisosComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  estado: boolean = true;

  constructor(
    private dialog:MatDialog,
    private rolesService:RolesService,
    private router:Router
  ) {

    
  }

  
  listRoles(roles){
    this.dataSource = new MatTableDataSource(roles)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.getRoles()
  }
  displayedColumns: string[] = ['nombre', 'descripcion', 'status', 'opciones'];
  dataSource
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getRoles(){
    this.rolesService.getRoles().subscribe((resp:any)=>{

      if(resp.ok){
        this.listRoles(resp.roles)
      }

    })
  }

  success(title, msg){

    this.dialog.open(SuccessDialogComponent, {
      width: '600px',
      data: {title, msg}
    })

  }

  nuevoRol(){
    
    const dialogRef = this.dialog.open(NuevoRoleComponent, {
      width: '600px'
    })
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getRoles()
        this.nuevoRolSuccess()
      }
    })
  }

  nuevoRolSuccess(){
    const dialogRef = this.dialog.open(NuevoRoleSuccessComponent, {
      width: '600px'
    })

    dialogRef.afterClosed().subscribe(result => {

    })
  }

  eliminarRol(id){

    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '600px'
    })

    dialogRef.afterClosed().subscribe(result => {
      
      if(result){
        this.rolesService.deleteRole(id, localStorage.getItem('id')).subscribe((resp:any) => {
                    
          if(resp.ok){
            this.success('Rol Eliminado', `Se elimino el rol ${resp.roles.nombre}`)
            this.getRoles();
          }
        
        })
      }
    })
  }

  cambiarStatus(id){
    this.rolesService.cambiarStatusRoleById({id, idAdmin: localStorage.getItem('id')}).subscribe((resp: any) => {
      
      if(resp.ok){
        this.success('Rol modificado', `Se elimino el rol ${resp.roles.nombre}`)
        this.getRoles()
      }

    })
  }

  editarRol(rol){
    const dialoigRef = this.dialog.open(EditarRoleComponent, {
      width: '600px',
      data: rol
    })

    dialoigRef.afterClosed().subscribe(result => {
      if(result){
        this.success('Rol Modificado', 'La categoria se modifico corerctamente')
        this.getRoles()
      }
    })
  }

  consultar(id){
    this.router.navigate(['/admin/configuracion/roles-permisos/asignar-role'], {state:{id}})
  }
  
}
