import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { NuevaCategoriaComponent } from '../configuracion-dialogs/nueva-categoria/nueva-categoria.component';
import { BorrarCategoriaComponent } from '../configuracion-dialogs/borrar-categoria/borrar-categoria.component';
import { EditarCategoriaComponent } from '../configuracion-dialogs/editar-categoria/editar-categoria.component';
import { CategoriaService } from '../../../globals-services/categoria.service';
import { NuevaCategoriaSuccessComponent } from '../configuracion-dialogs/nueva-categoria-success/nueva-categoria-success.component';
import { Router } from '@angular/router';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { BorrarComponent } from '../../../globals-dialogs/borrar/borrar.component';


@Component({
  selector: 'app-categorias-de-servicios',
  templateUrl: './categorias-de-servicios.component.html',
  styleUrls: ['./categorias-de-servicios.component.scss']
})
export class CategoriasDeServiciosComponent implements OnInit {
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(
    private dialog:MatDialog,
    private categoriasService:CategoriaService,
    private router:Router
  ) {

    
  }

  
  listCategorias(categorias){
    this.dataSource = new MatTableDataSource(categorias)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
  
    this.getCategorias()
  }
  displayedColumns: string[] = ['nombre', 'status', 'opciones'];
  dataSource
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getCategorias(){
    this.categoriasService.getCategorias().subscribe((resp:any)=>{
      if(resp.ok){
        this.listCategorias(resp.categorias)
        
      }
    })
  }


  subcategorias(categoria){
  
    this.router.navigate(['/admin/configuracion/categorias-servicio/subcategoria'],{state:categoria})
    
  }

  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }


  nuevaCategoria(){
 
    const dialogRef = this.dialog.open(NuevaCategoriaComponent, {
      width: '400px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getCategorias()
        this.nuevaCategoriaSuccess()
      }
    });
  }

  nuevaCategoriaSuccess(){
    const dialogRef = this.dialog.open(NuevaCategoriaSuccessComponent, {
      width: '400px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
     
    });
  }

  eliminarCategoria(id){
   
    
    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '350px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      
      if(result){
        this.categoriasService.deleteCategoria(id,localStorage.getItem('id')).subscribe((resp:any)=>{
            
          if(resp.ok){
            this.success('Categoria eliminada',`Se elimino la categoria ${resp.categoria.nombre}`)
            this.getCategorias()
          }
        })
      }
      
    });

  }

  estado = true

  cambiarStatus(id){
  
    this.categoriasService.cambiarStatusCategoriaByID({id,idAdmin:localStorage.getItem('id')}).subscribe((resp:any)=>{
      if(resp.ok){
        this.success('Categoria modificada',`Se cambio el estado a la categoria ${resp.categoria.nombre}`)
        this.getCategorias()
      }
    })
  
    
  }

  editarCategoria(categoria){

    const dialogRef = this.dialog.open(EditarCategoriaComponent, {
      width: '400px',
      data:categoria
      
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.success('Categoria modificada',`La categoria se modifico correctamente`)
        this.getCategorias()
      }
    });
  }


}
