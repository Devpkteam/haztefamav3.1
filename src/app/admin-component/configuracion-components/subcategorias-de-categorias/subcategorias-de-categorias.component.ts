import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { NuevaSubcategoriaComponent } from '../configuracion-dialogs/nueva-subcategoria/nueva-subcategoria.component';
import { BorrarSubcategoriaComponent } from '../configuracion-dialogs/borrar-subcategoria/borrar-subcategoria.component';
import { EditarSubcategoriaComponent } from '../configuracion-dialogs/editar-subcategoria/editar-subcategoria.component';
import { ActivatedRoute, Router } from '@angular/router';
import { SubcategoriaService } from '../../../globals-services/subcategoria.service';
import { NuevaCategoriaSuccessComponent } from '../configuracion-dialogs/nueva-categoria-success/nueva-categoria-success.component';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { BorrarComponent } from '../../../globals-dialogs/borrar/borrar.component';
export interface Categoria{
  _id:String
  nombre:String
  status:Boolean
  borrado:Boolean
}
export interface Subcategoria{
  _id:String
  nombre:String
  comision:Number
  status:Boolean
  borrado:Boolean
}

@Component({
  selector: 'app-subcategorias-de-categorias',
  templateUrl: './subcategorias-de-categorias.component.html',
  styleUrls: ['./subcategorias-de-categorias.component.scss']
})
export class SubcategoriasDeCategoriasComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  categoria:Categoria

  constructor(
    private dialog:MatDialog,
    private route:ActivatedRoute,
    private router:Router,
    private subcategoriasService:SubcategoriaService
  ) {
    console.log();
    if(this.router.getCurrentNavigation().extras.state == undefined){
      this.router.navigate(['/admin/configuracion/categorias-servicio'])
    }else{
      this.categoria = <Categoria>this.router.getCurrentNavigation().extras.state
    }
     
  }




  

 

  ngOnInit() {
    this.getSubcategorias()
  }

  displayedColumns: string[] = ['nombre','descripcion','creditos','comision', 'status', 'opciones'];
  dataSource

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  listSubcategorias(subcategorias){
    this.dataSource = new MatTableDataSource(subcategorias)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getSubcategorias(){
    console.log(this.categoria?._id);
    
    this.subcategoriasService.getSubcategoriasByCategoria(this.categoria?._id).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.listSubcategorias(resp.subcategorias)
        
      }
    })
  }
 

  
  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }


  nuevaSubcategoriaSuccess(){
    const dialogRef = this.dialog.open(NuevaCategoriaSuccessComponent, {
      width: '400px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
     
    });
  }

  nuevaSubcategoria(){
 
    const dialogRef = this.dialog.open(NuevaSubcategoriaComponent, {
      width: '400px',
      data:{categoria:this.categoria._id}
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getSubcategorias()
        this.nuevaSubcategoriaSuccess()
      }
    });
  }


  cambiarStatus(id){
  
    this.subcategoriasService.cambiarStatusSubcategoriaByID({id,idAdmin:localStorage.getItem('id')}).subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.success('Subcategoria modificada',`Se cambio el estado la de subcategoria ${resp.subcategoria.nombre}`)
        this.getSubcategorias()
      }
    })
  
    
  }

  editarSubcategoria(subcategoria){
    const dialogRef = this.dialog.open(EditarSubcategoriaComponent, {
      width: '400px',
      data:subcategoria
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.success('Subcategoria modificada',`Los cambios se han realizado correctamente`)
        this.getSubcategorias()
      }
    });
  }

  eliminarSubcategoria(id){

    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '350px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.subcategoriasService.deleteSubcategoria(id,localStorage.getItem('id')).subscribe((resp:any)=>{
          if(resp.ok){
            this.success('Subcategoria eliminada',`Se elimino la subcategoria ${resp.subcategoria.nombre}`)
            this.getSubcategorias()
          }
        })
      }
      
    });

  }



}
