import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ContratoService } from '../../../globals-services/contrato.service';
import { NuevaMembresiaComponent } from '../configuracion-dialogs/nueva-membresia/nueva-membresia.component';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { EditarMembresiaComponent } from '../configuracion-dialogs/editar-membresia/editar-membresia.component';
import { BorrarComponent } from '../../../globals-dialogs/borrar/borrar.component';
import { NuevoContratoComponent } from '../configuracion-dialogs/nuevo-contrato/nuevo-contrato.component';
import { EditarContratoComponent } from '../configuracion-dialogs/editar-contrato/editar-contrato.component';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.css']
})
export class ContratosComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(
    private dialog:MatDialog,
    private contratoService:ContratoService,
    private router:Router
  ) {

    
  }

  
  listarContratos(contrato){
    this.dataSource = new MatTableDataSource(contrato)    
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.getContrato()
  }

  displayedColumns: string[] = ['titulo', 'descripcion', 'destino','opciones'];
  dataSource
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getContrato(){
    this.contratoService.getContratos().subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.listarContratos(resp.contrato)
        
      }
    })
  }

  nuevaContrato(){
 
    const dialogRef = this.dialog.open(NuevoContratoComponent, {
      width: '600px',
      disableClose:true
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getContrato()
        this.success('Contrato creada','El contrato se ha registrado correctamente!')
      }
    });
  }


  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

  eliminarContrato(id){

    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '400px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      
      if(result){
        this.contratoService.deleteContrato(id,localStorage.getItem('id')).subscribe((resp:any)=>{
            
          if(resp.ok){
            this.success('Contrato eliminado',`Se elimino el Contrato ${resp.contrato.titulo}`)
            this.getContrato()
          }
        })
      }
      
    });

  }

  editarContrato(data){

    const dialogRef = this.dialog.open(EditarContratoComponent, {
      width: '600px',
      data
      
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.success('Cambios realizados','El Contrato se modifico con éxito')
        this.getContrato()
      }
    });
  }

}
