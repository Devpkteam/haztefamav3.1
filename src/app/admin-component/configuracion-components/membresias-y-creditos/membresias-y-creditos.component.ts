import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { NuevaCategoriaComponent } from '../configuracion-dialogs/nueva-categoria/nueva-categoria.component';
import { BorrarCategoriaComponent } from '../configuracion-dialogs/borrar-categoria/borrar-categoria.component';
import { EditarCategoriaComponent } from '../configuracion-dialogs/editar-categoria/editar-categoria.component';
import { CategoriaService } from '../../../globals-services/categoria.service';
import { NuevaCategoriaSuccessComponent } from '../configuracion-dialogs/nueva-categoria-success/nueva-categoria-success.component';
import { Router } from '@angular/router';
import { MembresiaService } from '../../../globals-services/membresia.service';
import { NuevaMembresiaComponent } from '../configuracion-dialogs/nueva-membresia/nueva-membresia.component';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { EditarMembresiaComponent } from '../configuracion-dialogs/editar-membresia/editar-membresia.component';
import { BorrarComponent } from '../../../globals-dialogs/borrar/borrar.component';

@Component({
  selector: 'app-membresias-y-creditos',
  templateUrl: './membresias-y-creditos.component.html',
  styleUrls: ['./membresias-y-creditos.component.scss']
})
export class MembresiasYCreditosComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(
    private dialog:MatDialog,
    private membresiaService:MembresiaService,
    private router:Router
  ) {

    
  }

  
  listarMembresias(membresias){
    this.dataSource = new MatTableDataSource(membresias)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
  
    this.getMembresias()
  }

  displayedColumns: string[] = ['nombre', 'creditos', 'precio','descripcion','status','opciones'];
  dataSource
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getMembresias(){
    this.membresiaService.getMembresias().subscribe((resp:any)=>{
      console.log(resp);
      
      if(resp.ok){
        this.listarMembresias(resp.membresias)
        
      }
    })
  }



  nuevaMembresia(){
 
    const dialogRef = this.dialog.open(NuevaMembresiaComponent, {
      width: '400px',
      disableClose:true
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getMembresias()
        this.success('Membresia creada','La membresia se ha registrado correctamente!')
      }
    });
  }


  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

  nuevaCategoriaSuccess(){
    const dialogRef = this.dialog.open(NuevaCategoriaSuccessComponent, {
      width: '400px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
     
    });
  }

  eliminarMembresia(id){

    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '350px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      
      if(result){
        this.membresiaService.deleteMembresia(id,localStorage.getItem('id')).subscribe((resp:any)=>{
            
          if(resp.ok){
            this.success('Membresia eliminada',`Se elimino la membresia ${resp.membresia.nombre}`)
            this.getMembresias()
          }
        })
      }
      
    });

  }

  estado = true

  cambiarStatus(id){
  
    this.membresiaService.updateStatusMembresia({id,idAdmin:localStorage.getItem('id')}).subscribe((resp:any)=>{
      console.log();
      
      if(resp.ok){
        this.success('Cambio realizado',`Se cambio el estado a la membresia ${resp.membresia.nombre}`)
        this.getMembresias()
      }
    })
  
    
  }

  editarMembresia(data){

    const dialogRef = this.dialog.open(EditarMembresiaComponent, {
      width: '400px',
      data
      
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.success('Cambios realizados','La membresia se modifico con éxito')
        this.getMembresias()
      }
    });
  }


}
