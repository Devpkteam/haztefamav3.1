import { Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ConflictosComponent } from './conflictos/conflictos.component';
import { PagosComponent } from './pagos/pagos.component';
import { RolesYPermisosComponent } from './configuracion-components/roles-y-permisos/roles-y-permisos.component';
import { CategoriasDeServiciosComponent } from './configuracion-components/categorias-de-servicios/categorias-de-servicios.component';
import { MembresiasYCreditosComponent } from './configuracion-components/membresias-y-creditos/membresias-y-creditos.component';
import { AtencionAlClienteComponent } from './servicios-clientes-components/atencion-al-cliente/atencion-al-cliente.component';
import { ComentariosYSugerenciasComponent } from './servicios-clientes-components/comentarios-y-sugerencias/comentarios-y-sugerencias.component';
import { EstadisticasDelSitioComponent } from './auditoria-components/estadisticas-del-sitio/estadisticas-del-sitio.component';
import { ReportesComponent } from './auditoria-components/reportes/reportes.component';
import { AuditoriaInternaComponent } from './auditoria-components/auditoria-interna/auditoria-interna.component';
import { AdminPerfilComponent } from './auditoria-components/admin-perfil/admin-perfil.component';
import { AdminUsuarioComponent } from './auditoria-components/admin-usuario/admin-usuario.component';
import { AdminSeguridadComponent } from './admin-seguridad/admin-seguridad.component';
import { AuthGuardAdminService } from '../guards/auth-guard-admin.service';
import { SubcategoriasDeCategoriasComponent } from './configuracion-components/subcategorias-de-categorias/subcategorias-de-categorias.component';
import { AdminNotificationsComponent } from './admin-notifications/admin-notifications.component';
import { UsuariosComponent } from './usuarios-components/usuarios/usuarios.component';
import { NuevoAdministradorComponent } from './usuarios-components/nuevo-administrador/nuevo-administrador.component';
import { ConsultarUsuarioComponent } from './usuarios-components/consultar-usuario/consultar-usuario.component';
import { AsignarRolesComponent } from './configuracion-components/asignar-roles/asignar-roles.component';
import { ContratosComponent } from './configuracion-components/contratos/contratos.component';
import { DetalleHistorialPagoComponent } from './detalle-historial-pago/detalle-historial-pago.component';



export const AdminRoutes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent,
    data:{
      title:'Inicio',
      breadcrumbs:['Inicio']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'usuarios',
    component: UsuariosComponent,
    data:{
      title:'Usuarios',
      breadcrumbs:['Usuarios']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'usuarios/agregar',
    component: NuevoAdministradorComponent,
    data:{
      title:'Agregar',
      breadcrumbs:['Usuarios','Nuevo administrador']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'usuarios/consultar',
    component: ConsultarUsuarioComponent,
    data:{
      title:'Consultar',
      breadcrumbs:['Usuarios','Consultar']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'servicios',
    component: ServiciosComponent,
    data:{
      title:'Servicios',
      breadcrumbs:['Servicios']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'conflictos',
    component: ConflictosComponent,
    data:{
      title:'Conflictos',
      breadcrumbs:['Conflictos']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'pagos',
    component: PagosComponent,
    data:{
      title:'Pagos',
      breadcrumbs:['Pagos']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'pagos/detalle-historial-pago',
    component: DetalleHistorialPagoComponent,
    data:{
      title:'Pagos',
      breadcrumbs:['Pagos']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'configuracion/roles-permisos',
    component: RolesYPermisosComponent,
    data:{
      title:'Roles y Permisos',
      breadcrumbs:['Configuracion','Roles y Permisos']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'configuracion/roles-permisos/asignar-role',
    component: AsignarRolesComponent,
    data:{
      title:'Asignar Roles y Permisos',
      breadcrumbs:['Configuracion','Roles y Permisos', 'Asignar Rol']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'configuracion/categorias-servicio',
    component: CategoriasDeServiciosComponent,
    data:{
      title:'Categorias de servicios',
      breadcrumbs:['Configuracion','Categorias de servicios']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'configuracion/categorias-servicio/subcategoria',
    component: SubcategoriasDeCategoriasComponent,
    data:{
      title:'Subcategorias',
      breadcrumbs:['Configuracion','Categorias de servicios','Subcategorias']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'configuracion/membresias-credito',
    component: MembresiasYCreditosComponent,
    data:{
      title:'Membresias de credito',
      breadcrumbs:['Configuracion','Membresias de credito']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'configuracion/contratos',
    component: ContratosComponent,
    data:{
      title:'Contratos',
      breadcrumbs:['Configuracion','Contratos']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'servicios-cliente/atencion-cliente',
    component: AtencionAlClienteComponent,
    data:{
      title:'Atencion al cliente',
      breadcrumbs:['Servicios al cliente','Atencion al cliente']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'servicios-cliente/comentarios-sugerencias',
    component: ComentariosYSugerenciasComponent,
    data:{
      title:'Comentarios y sugerencias',
      breadcrumbs:['Servicios al cliente','Comentarios y sugerencias']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'auditoria/estadisticas',
    component: EstadisticasDelSitioComponent,
    data:{
      title:'Estadisticas',
      breadcrumbs:['Auditoria','Estadisticas']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'auditoria/reportes',
    component: ReportesComponent,
    data:{
      title:'Reportes',
      breadcrumbs:['Auditoria','Reportes']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'auditoria/interna',
    component: AuditoriaInternaComponent,
    data:{
      title:'Auditoria interna',
      breadcrumbs:['Auditoria','Auditoria interna']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'perfil',
    component: AdminPerfilComponent,
    data:{
      title:'Perfil',
      breadcrumbs:['Usuario','Perfil']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'usuario',
    component: AdminUsuarioComponent,
    data:{
      title:'Usuario',
      breadcrumbs:['Usuario']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'notificaciones',
    component: AdminNotificationsComponent,
    data:{
      title:'Notificaciones',
      breadcrumbs:['Notificaciones']
    },
    canActivate:[AuthGuardAdminService]
  },
  {
    path: 'seguridad',
    component: AdminSeguridadComponent,
    data:{
      title:'Seguridad',
      breadcrumbs:['Usuario','Seguridad']
    },
    canActivate:[AuthGuardAdminService]
  },
];
