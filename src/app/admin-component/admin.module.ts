import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';

import { DemoMaterialModule } from '../demo-material-module';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AdminRoutes } from './admin.routing';
import { ServiciosComponent } from './servicios/servicios.component';
import { ConflictosComponent } from './conflictos/conflictos.component';
import { PagosComponent } from './pagos/pagos.component';
import { RolesYPermisosComponent } from './configuracion-components/roles-y-permisos/roles-y-permisos.component';
import { CategoriasDeServiciosComponent } from './configuracion-components/categorias-de-servicios/categorias-de-servicios.component';
import { MembresiasYCreditosComponent } from './configuracion-components/membresias-y-creditos/membresias-y-creditos.component';
import { AtencionAlClienteComponent } from './servicios-clientes-components/atencion-al-cliente/atencion-al-cliente.component';
import { ComentariosYSugerenciasComponent } from './servicios-clientes-components/comentarios-y-sugerencias/comentarios-y-sugerencias.component';
import { EstadisticasDelSitioComponent } from './auditoria-components/estadisticas-del-sitio/estadisticas-del-sitio.component';
import { ReportesComponent } from './auditoria-components/reportes/reportes.component';
import { AuditoriaInternaComponent } from './auditoria-components/auditoria-interna/auditoria-interna.component';
import { AdminPerfilComponent } from './auditoria-components/admin-perfil/admin-perfil.component';
import { AdminUsuarioComponent } from './auditoria-components/admin-usuario/admin-usuario.component';
import { AdminSeguridadComponent } from './admin-seguridad/admin-seguridad.component';
import { SubcategoriasDeCategoriasComponent } from './configuracion-components/subcategorias-de-categorias/subcategorias-de-categorias.component';
import { NuevaCategoriaComponent } from './configuracion-components/configuracion-dialogs/nueva-categoria/nueva-categoria.component';
import { BorrarCategoriaComponent } from './configuracion-components/configuracion-dialogs/borrar-categoria/borrar-categoria.component';
import { EditarCategoriaComponent } from './configuracion-components/configuracion-dialogs/editar-categoria/editar-categoria.component';
import { NuevaSubcategoriaComponent } from './configuracion-components/configuracion-dialogs/nueva-subcategoria/nueva-subcategoria.component';
import { BorrarSubcategoriaComponent } from './configuracion-components/configuracion-dialogs/borrar-subcategoria/borrar-subcategoria.component';
import { EditarSubcategoriaComponent } from './configuracion-components/configuracion-dialogs/editar-subcategoria/editar-subcategoria.component';
import { NuevaCategoriaSuccessComponent } from './configuracion-components/configuracion-dialogs/nueva-categoria-success/nueva-categoria-success.component';
import { NuevaSubcategoriaSuccessComponent } from './configuracion-components/configuracion-dialogs/nueva-subcategoria-success/nueva-subcategoria-success.component';
import { AdminNotificationsComponent } from './admin-notifications/admin-notifications.component';
import { CambiosPerfilSuccessComponent } from './auditoria-components/perfil-dialogs/cambios-perfil-success/cambios-perfil-success.component';
import { PasswordInvalidComponent } from './auditoria-components/perfil-dialogs/password-invalid/password-invalid.component';
import { ErrorDialogComponent } from '../globals-dialogs/error-dialog/error-dialog.component';
import { SuccessDialogComponent } from '../globals-dialogs/success-dialog/success-dialog.component';
import { NuevaMembresiaComponent } from './configuracion-components/configuracion-dialogs/nueva-membresia/nueva-membresia.component';
import { EditarMembresiaComponent } from './configuracion-components/configuracion-dialogs/editar-membresia/editar-membresia.component';
import { UsuariosComponent } from './usuarios-components/usuarios/usuarios.component';
import { BorrarComponent } from '../globals-dialogs/borrar/borrar.component';
import { NuevoAdministradorComponent } from './usuarios-components/nuevo-administrador/nuevo-administrador.component';
import { ConsultarUsuarioComponent } from './usuarios-components/consultar-usuario/consultar-usuario.component';
import { NuevoRoleSuccessComponent } from './configuracion-components/configuracion-dialogs/nuevo-role-success/nuevo-role-success.component';
import { NuevoRoleComponent } from './configuracion-components/configuracion-dialogs/nuevo-role/nuevo-role.component';
import { EditarRoleComponent } from './configuracion-components/configuracion-dialogs/editar-role/editar-role.component';
import { AsignarRolesComponent } from './configuracion-components/asignar-roles/asignar-roles.component';
import { ContratosComponent } from './configuracion-components/contratos/contratos.component';
import { NuevoContratoComponent } from './configuracion-components/configuracion-dialogs/nuevo-contrato/nuevo-contrato.component';
import { EditarContratoComponent } from './configuracion-components/configuracion-dialogs/editar-contrato/editar-contrato.component';
import { DetalleHistorialPagoComponent } from './detalle-historial-pago/detalle-historial-pago.component';
import { RespuestaComentarioComponent } from './servicios-clientes-components/respuesta-comentario/respuesta-comentario.component';
import { EditarRespuestaComentarioComponent } from './servicios-clientes-components/editar-respuesta-comentario/editar-respuesta-comentario.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminRoutes),
    DemoMaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  entryComponents: [],
  declarations: [
    ServiciosComponent,
    ConflictosComponent,
    PagosComponent,
    RolesYPermisosComponent,
    CategoriasDeServiciosComponent,
    MembresiasYCreditosComponent,
    AtencionAlClienteComponent,
    ComentariosYSugerenciasComponent,
    EstadisticasDelSitioComponent,
    ReportesComponent,
    AuditoriaInternaComponent,
    AdminPerfilComponent,
    AdminUsuarioComponent,
    AdminSeguridadComponent,
    SubcategoriasDeCategoriasComponent,
    NuevaCategoriaComponent,
    BorrarCategoriaComponent,
    EditarCategoriaComponent,
    NuevaSubcategoriaComponent,
    BorrarSubcategoriaComponent,
    EditarSubcategoriaComponent,
    NuevaCategoriaSuccessComponent,
    NuevaSubcategoriaSuccessComponent,
    AdminNotificationsComponent,
    CambiosPerfilSuccessComponent,
    PasswordInvalidComponent,
    ErrorDialogComponent,
    SuccessDialogComponent,
    NuevaMembresiaComponent,
    EditarMembresiaComponent,
    UsuariosComponent,
    BorrarComponent,
    NuevoAdministradorComponent,
    ConsultarUsuarioComponent,
    NuevoRoleSuccessComponent,
    NuevoRoleComponent,
    EditarRoleComponent,
    AsignarRolesComponent,
    ContratosComponent,
    NuevoContratoComponent,
    EditarContratoComponent,
    DetalleHistorialPagoComponent,    
    EditarContratoComponent,   
    RespuestaComentarioComponent, 
    EditarRespuestaComentarioComponent
    
  ]
})
export class AdminComponentsModule {}
