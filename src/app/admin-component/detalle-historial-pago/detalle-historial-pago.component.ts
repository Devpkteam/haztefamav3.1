import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagosService } from '../../globals-services/pagos.service';

@Component({
  selector: 'app-detalle-historial-pago',
  templateUrl: './detalle-historial-pago.component.html',
  styleUrls: ['./detalle-historial-pago.component.css']
})
export class DetalleHistorialPagoComponent implements OnInit {
  historial:any;
  id:any;
  url:any;
  constructor(
    private router: Router,
    private _pagos: PagosService
  ) { 
    if(this.router.getCurrentNavigation().extras.state == undefined){
      this.router.navigate(['/admin/pagos/'])
    }else{
    this.id = this.router.getCurrentNavigation().extras.state.id
    this.historial = {}
    }
  }

  ngOnInit(): void {
    this.getHistorial()
  }
  getHistorial(){
    this._pagos.getHistorialId(this.id).subscribe((resp:any) => {
      if(resp.ok){
        this.url = resp.url
        this.historial = resp.historial[0]
        console.log(this.historial)
      }
    })

  }
  formatRole(role){
    switch (role) {
      case 'admin':
        return 'Administrador'
        break;
      case 'client':
        return 'Cliente'
        break;
    
      case 'provider':
        return 'Proveedor'
        break;
    
      default:
        break;
    }
  }

}
