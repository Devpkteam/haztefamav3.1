import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleHistorialPagoComponent } from './detalle-historial-pago.component';

describe('DetalleHistorialPagoComponent', () => {
  let component: DetalleHistorialPagoComponent;
  let fixture: ComponentFixture<DetalleHistorialPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleHistorialPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleHistorialPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
