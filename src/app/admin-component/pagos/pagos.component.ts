import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { PagosService } from "../../globals-services/pagos.service";
import { Socket } from "ngx-socket-io";
import { FormBuilder, FormGroup } from "@angular/forms";
import * as FileSaver from "file-saver";
import { Router } from "@angular/router";

@Component({
  selector: "app-pagos",
  templateUrl: "./pagos.component.html",
  styleUrls: ["./pagos.component.scss"],
})
export class PagosComponent implements OnInit {
  since: any = "";
  until: any = "";
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private pagosServices: PagosService,
    private socket: Socket,
    private fb: FormBuilder,
    private router: Router
  ) {}
  formFilterPagos = this.fb.group(
    {
      since: [""],
      until: [""],
    },
    { validators: this.checkvalues }
  );

  checkvalues(form: FormGroup) {
    console.log(form.controls);
  }

  listarHistorial(historial) {
    this.dataSource = new MatTableDataSource(historial);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.getHistorial();
    this.socket.on("Pagos", () => {
      this.getHistorial();
    });
  }

  displayedColumns: string[] = [
    "usuario",
    "accion",
    "monto",
    "fecha",
    "detalles",
  ];
  dataSource;
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getHistorial() {
    this.pagosServices.getHistorial().subscribe((resp: any) => {
      console.log(resp);

      if (resp.ok) {
        this.listarHistorial(resp.historial);
      }
    });
  }

  toExcel() {
    this.since = this.formFilterPagos.controls.since.value;
    this.until = this.formFilterPagos.controls.until.value;
    this.pagosServices
      .toExcel({ since: this.since, until: this.until })
      .subscribe((resp: any) => {
        console.log(resp);

        const date = Date.now();
        FileSaver.saveAs(resp, `historialPagos${date}.xlsx`);
        if (resp.ok) {
        }
      });
  }
  onSearchChange(value) {
    this.since = value.since.value;
    console.log(this.since);
    this.until = value.until.value;
    console.log(this.until);
  }
  info(id: any): void {
    this.router.navigate(["/admin/pagos/detalle-historial-pago"], {
      state: { id },
    });
  }
}
