import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { BitacoraService } from '../../../globals-services/bitacora.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as FileSaver from 'file-saver';


@Component({
  selector: 'app-auditoria-interna',
  templateUrl: './auditoria-interna.component.html',
  styleUrls: ['./auditoria-interna.component.scss']
})
export class AuditoriaInternaComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  dataSource;
  displayedColumns: string[] = ['Usuario', 'Accion','Fecha'];
  query: string = "";
  since: any;
  until: any;

  constructor(
    private dialog: MatDialog,
    private bitacoraServices: BitacoraService,
    private router: Router,
    private fb: FormBuilder,
  ) { }

  filterForm = this.fb.group({
    query: [''],
    since: [''],
    until: [''],

  }, {validators:this.checkvalues})

  checkvalues(form: FormGroup){
    console.log(form.controls);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {

    this.getBitacora()

  }

  listarBitacora(bitacora){
    this.dataSource = new MatTableDataSource(bitacora)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getBitacora(){
    this.bitacoraServices.getAllBitacora().subscribe((resp: any) => {
      console.log(resp);
      
      if (resp.ok) {
        this.listarBitacora(resp.historial)
      }
    })
  }

  getBitacoraFilter(){
    this.bitacoraServices.getBitacoraFilter({query:this.query, since: this.since, until: this.until}).subscribe((resp: any) => {      
      console.log(resp);
      
      if (resp.ok) {
        console.log(resp.historial);
        
        this.listarBitacora(resp.historial)

      }
    })
  }

  toExcel(){
    this.bitacoraServices.toExcel().subscribe((resp: any) =>{
      console.log(resp);
      
      const date = Date.now()
      FileSaver.saveAs(resp, `auditoria${date}.xlsx`)
      if (resp.ok) {
      }
    }) 
  }

  onSearchChange(value){
    this.query = value.query;
    console.log(this.query);
    this.since = value.since;
    console.log(this.since);
    this.until = value.until;
    console.log(this.until);
    
    this.getBitacoraFilter()
  }



}
