import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';

import { Router } from '@angular/router';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { UsuarioService } from '../../../globals-services/usuario.service';
import { BorrarCategoriaComponent } from '../../configuracion-components/configuracion-dialogs/borrar-categoria/borrar-categoria.component';
import { BorrarComponent } from '../../../globals-dialogs/borrar/borrar.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(
    private dialog:MatDialog,
    private usuarioService:UsuarioService,
    private router:Router
  ) {

    
  }

  
  listarUsuarios(usuarios){
    this.dataSource = new MatTableDataSource(usuarios)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
  
    this.getUsuarios()
  }

  displayedColumns: string[] = ['nombre', 'apellido', 'role','fecha','status','verificado','opciones'];
  dataSource
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getUsuarios(){
    this.usuarioService.getUsers().subscribe((resp:any)=>{
      
      if(resp.ok){
        this.listarUsuarios(resp.usuarios)
      }
    })
  }

  formatRole(role){
    switch (role) {
      case 'admin':
        return 'Administrador'
        break;
      case 'client':
        return 'Cliente'
        break;
    
      case 'provider':
        return 'Proveedor'
        break;
    
      default:
        break;
    }
  }


  nuevaMembresia(){
 
    // const dialogRef = this.dialog.open(NuevaMembresiaComponent, {
    //   width: '400px'
  
    // });
  
    // dialogRef.afterClosed().subscribe(result => {
    //   if(result){
    //     this.getMembresias()
    //     this.success('Membresia creada','La membresia se ha registrado correctamente!')
    //   }
    // });
  }


  success(title,msg){
 
    this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      data:{title,msg}
    });
  
  }

  nuevaCategoriaSuccess(){
    // const dialogRef = this.dialog.open(NuevaCategoriaSuccessComponent, {
    //   width: '400px'
  
    // });
  
    // dialogRef.afterClosed().subscribe(result => {
     
    // });
  }

  eliminar(id){

    const dialogRef = this.dialog.open(BorrarComponent, {
      width: '350px'
  
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      
      if(result){
        this.usuarioService.delete(id,localStorage.getItem('id')).subscribe((resp:any)=>{
            
          if(resp.ok){
            this.success('Usuario eliminado',`Se elimino al usuario ${resp.usuario.username}`)
            this.getUsuarios()
          }
        })
      }
      
    });

  }

  consultar(id){
    this.router.navigate(['/admin/usuarios/consultar'],{state:{id}})
  }

  estado = true

  cambiarStatus(id){
  
    this.usuarioService.updateStatusUsuario({id,idAdmin:localStorage.getItem('id')}).subscribe((resp:any)=>{
      console.log();
      
      if(resp.ok){
        this.success('Cambio realizado',`Se cambio el estado del usuario ${resp.usuario.username}`)
        this.getUsuarios()
      }
    })
  
    
  }

  editarMembresia(data){

    // const dialogRef = this.dialog.open(EditarMembresiaComponent, {
    //   width: '400px',
    //   data
      
    // });
  
    // dialogRef.afterClosed().subscribe(result => {
    //   if(result){
    //     this.success('Cambios realizados','La membresia se modifico con éxito')
    //     this.getMembresias()
    //   }
    // });
  }



}
