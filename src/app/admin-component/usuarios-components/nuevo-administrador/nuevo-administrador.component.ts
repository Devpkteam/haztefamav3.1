import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { RegisterService } from '../../../registro-components/registro-servicios/register.service';
import { EmailRepeatComponent } from '../../../registro-components/registro-dialogs/email-repeat/email-repeat.component';
import { MatDialog } from '@angular/material';
import { SuccessDialogComponent } from '../../../globals-dialogs/success-dialog/success-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-administrador',
  templateUrl: './nuevo-administrador.component.html',
  styleUrls: ['./nuevo-administrador.component.scss']
})
export class NuevoAdministradorComponent implements OnInit {

  constructor(
    private fb:FormBuilder,
    private registroService:RegisterService,
    private dialog:MatDialog,
    private router:Router
  ) { }

  ngOnInit() {
  }

  clienteForm = this.fb.group( {

    nombre: ['', [Validators.required,  Validators.maxLength(40) ,Validators.pattern("^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$")] ],
    apellido: ['', [Validators.required,  Validators.maxLength(40) ,Validators.pattern("^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$")] ],
    email: ['', [Validators.required, Validators.maxLength(50) ,Validators.pattern("[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,}")] ],  
    username: ['', [Validators.required, Validators.maxLength(50) ] ],
    password: [ '',  [ Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}[^'\s]/) ] ],
    rpassword: [ '',  [ Validators.required, Validators.minLength(8) ] ],
    telefono: [ '',  [Validators.required, Validators.minLength(8),  Validators.maxLength(11), Validators.pattern("[0-9]{9,11}") ] ],
    direccion: [ '', Validators.required ]

 },{validator: this.checkPasswords })


 checkPasswords(form: FormGroup) { // funcion syncrona para verificar que las contraseñas coinciden
  let pass = form.controls.password.value;
  let confirmPass = form.controls.rpassword.value;
  if(pass !== confirmPass){
    form.controls.rpassword.setErrors({ 'repeatInvalid' : true })
  }


  return null     
}

resolved(captchaResponse: string) {
  console.log(`Resolved response token: ${captchaResponse}`);
 
}

register(){
  let cliente = this.clienteForm.value
  cliente.role = 'admin'
  cliente.idAdmin = localStorage.getItem('id')

  this.registroService.register(cliente).subscribe((resp:any)=>{
    console.log(resp);
    
    if(resp.ok){
   
      this.success('Administrador Registrado',`Se registro el administrador con el nombre de usuario ${resp.user.username}`)
      this.router.navigate(['/admin/usuarios'])
    }else{
      if(resp.err.code == 11000){
        this.fieldRepeat(resp.index)
        
      }else{
        
      }
    }
    
  })
  
}

fieldRepeat(index){
  let data
  switch (index) {
    case 'email':
        data = {title:'Correo en uso',msg:'El correo electronico ya se encuentra registrado'}
      break;
    
    case 'username':
        data = {title:'Usuario en uso',msg:'El nombre de usuario ya se encuentra registrado'}
      break;
    
    default:
      break;
  }
  const dialogRef = this.dialog.open(EmailRepeatComponent, {

    width: '500px',
    data
  });

  dialogRef.afterClosed().subscribe(result => {

  });
}

success(title,msg){
 
  this.dialog.open(SuccessDialogComponent, {
    width: '500px',
    data:{title,msg}
  });

}

}
