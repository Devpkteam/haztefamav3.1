/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthGuardClientService } from './auth-guard-client.service';

describe('Service: AuthGuardClient', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuardClientService]
    });
  });

  it('should ...', inject([AuthGuardClientService], (service: AuthGuardClientService) => {
    expect(service).toBeTruthy();
  }));
});
