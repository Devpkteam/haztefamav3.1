/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthGuardProviderService } from './auth-guard-provider.service';

describe('Service: AuthGuardProvider', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuardProviderService]
    });
  });

  it('should ...', inject([AuthGuardProviderService], (service: AuthGuardProviderService) => {
    expect(service).toBeTruthy();
  }));
});
