import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SessionGuardService {

constructor(
  private router:Router,
  private socket:Socket
) { }
canActivate(
  next: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    if (localStorage.getItem('id')) {
      if(localStorage.getItem('role')){
        this.socket.emit('registerID',{id:localStorage.getItem('id')})
        console.log(localStorage.getItem('role'));
        
        switch (localStorage.getItem('role')) {

          case 'admin':
             this.socket.emit('logAdmin')
            
              this.router.navigate(['/admin'])
            break;
          case 'client':
              this.router.navigate(['/cliente'])
            break;
          case 'provider':
              this.router.navigate(['/proveedor'])
            break;
          default:
            localStorage.clear()
            this.router.navigate(['/'])
            break;
        }
      }else{
        localStorage.clear()
            this.router.navigate(['/'])
      }
      
      return false;
    }else{
      return true;
    }
   
    
}

}
