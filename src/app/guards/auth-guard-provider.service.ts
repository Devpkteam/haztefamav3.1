import { Injectable } from '@angular/core';
import { take, map } from 'rxjs/operators';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { GuardService } from './guard.service';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardProviderService {

  constructor(
    private router:Router,
    private guardService:GuardService,
       private socket:Socket
    ) { }
  
  
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    
      return this.guardService.getUser(localStorage.getItem('id')).pipe(
        take(1),
        map((resp:any)=>{
          

          if(resp.ok){
            if(resp.data.role == 'provider'){
                 this.socket.emit('registerID',{id:localStorage.getItem('id')})
              return true
            }else{
              return false
            }
           
          }else{
            localStorage.clear()
            this.router.navigate(['/']);
            return false
          }
          
        })
      )
  }

}
